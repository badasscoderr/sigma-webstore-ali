/** Initilizing Vue App */
const app = Vue.createApp({
	template: /* html */ `
	  <header>
			<div class="offcanvas-overlay" :style="[overlay]"></div>
			<Navigation />
		</header>
		<router-view></router-view>
		<footer>
			<Footer />
		</footer>
	`,
	components: { Navigation, Footer },
	computed: {
		...Vuex.mapGetters([
			"getModalState",
			"getCartSidebarState",
			"getMobileMenuState",
		]),

		overlay() {
			if (
				this.getModalState ||
				this.getCartSidebarState ||
				this.getMobileMenuState
			) {
				return {
					display: "block",
				};
			}
		},
	},

	data() {},

	mounted() {
		this.$store.commit("UPDATE_CART");
	},

	methods: {},
});

const clickOutside = {};

app.directive("click-outside", clickOutside);

/** Registering Shared components */
app.component("AccountAction", AccountAction);
app.component("MobileMenu", MobileMenu);
app.component("SearchModal", SearchModal);
app.component("CartSidebar", CartSidebar);
app.component("Breadcrumb", Breadcrumb);
app.component("ProductCard", ProductCard);
app.component("LargeProductCard", LargeProductCard);

/** Registering Views */
app.component("Home", Home);
app.component("Shop", Shop);
