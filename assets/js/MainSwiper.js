const swiper = new Swiper(".main-slider", {
	loop: true,
	autoplay: {
		delay: 2500,
		disableOnInteraction: false,
		pauseOnMouseEnter: true,
	},
});
