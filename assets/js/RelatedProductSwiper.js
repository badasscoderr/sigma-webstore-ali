const RelatedProductSwiper = {
	slidesPerView: 4,
	spaceBetween: 20,
	speed: 1500,
	loop: true,
	navigation: {
		//nextEl: ".swiper-button-next",
		//prevEl: ".swiper-button-prev",
	},
	breakpoints: {
		0: {
			slidesPerView: 1,
		},
		478: {
			slidesPerView: 1,
		},
		576: {
			slidesPerView: 2,
		},
		768: {
			slidesPerView: 3,
		},
		992: {
			slidesPerView: 3,
		},
		1024: {
			slidesPerView: 4,
		},
		1200: {
			slidesPerView: 4,
		},
	},
};

new Swiper(".related-product-carousel", RelatedProductSwiper);
