const CategorySwiper = {
	slidesPerView: 6,
	spaceBetween: 30,
	autoplay: false,
	breakpoints: {
		0: {
			slidesPerView: 2,
		},
		478: {
			slidesPerView: 2,
		},
		576: {
			slidesPerView: 3,
		},
		768: {
			slidesPerView: 4,
		},
		992: {
			slidesPerView: 5,
		},
		1200: {
			slidesPerView: 6,
		},
	},
};

new Swiper(".category-carousel", CategorySwiper);
