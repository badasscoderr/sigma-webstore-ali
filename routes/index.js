/** Initilizing Vue Router */
const router = VueRouter.createRouter({
	history: VueRouter.createWebHistory("/"),
	routes: [
		{
			name: "Home",
			path: "/",
			component: Home,
		},
		{
			name: "Shop",
			path: "/shop",
			component: Shop,
		},
		{
			name: "Product",
			path: "/products/:id",
			component: Product,
			props: true,
		},
		{
			name: "Cart",
			path: "/cart",
			component: Cart,
		},
		{
			name: "Checkout",
			//path: "/products/:id/checkout",
			path: "/checkout",
			component: Checkout,
			props: true,
		},
		{
			name: "ThankYou",
			path: "/thank-you",
			component: ThankYou,
		},
		{
			path: "/account",
			name: "Account",
			component: Account,
		},
		{
			path: "/auth",
			name: "Auth",
			component: Auth,
		},
		{
			path: "/:pathMatch(.*)*",
			name: "NotFound",
			component: NotFound,
		},
	],
	scrollBehavior(to, from, savedPosition) {
		if (savedPosition) {
			return savedPosition;
		} else {
			return { top: 0 };
		}
	},
});
