function setLocalStorage(cart) {
	localStorage.setItem("cart", JSON.stringify(cart));
}

// Initilizing Vuex Store
const store = new Vuex.Store({
	state: {
		showSearchModal: false,
		showCartSidebar: false,
		showMobileMenu: false,
		showActionDropdown: false,
		ssl: "valid",
		products: [],
		productsPerPage: [],
		categories: [],
		cart: [],
		cartCount: 0,
		cartProduct: [],
		user: null,
		isNewUser: true,

		users: [],
	},
	getters: {
		getModalState: (state) => state.showSearchModal,
		getCartSidebarState: (state) => state.showCartSidebar,
		getMobileMenuState: (state) => state.showMobileMenu,
		getActionDropdownState: (state) => state.showActionDropdown,
		getSslState: (state) => state.ssl,
		getProducts: (state) => state.products,
		getProductsPerPage: (state) => state.productsPerPage,
		getCategories: (state) => state.categories,
		getCart: (state) => state.cart,
		getCartProduct: () => state.cartProduct,
		getProductQty: (state) => (payload) => {
			const item = state.cart.find((i) => i.id === payload.id);

			if (item) {
				return item.quantity;
			} else {
				return null;
			}
		},
		getCartCount: (state) => state.cartCount,
		getUser: (state) => state.user,

		allUsers: (state) => state.users,
	},

	mutations: {
		TOGGLE_SEARCH_MODAL(state) {
			state.showSearchModal = !state.showSearchModal;
		},
		TOGGLE_CART_SIDEBAR(state) {
			state.showCartSidebar = !state.showCartSidebar;
		},
		TOGGLE_MOBILE_MENU(state) {
			state.showMobileMenu = !state.showMobileMenu;
		},
		TOGGLE_ACTION_DROPDOWN(state) {
			state.showActionDropdown = !state.showActionDropdown;
		},
		SET_PRODUCTS(state, payload) {
			state.products = payload;
		},
		SET_PRODUCTS_PER_PAGE(state, payload) {
			state.productsPerPage = payload;
		},
		SET_CATEGORIES(state, payload) {
			state.categories = payload;
		},

		ADD_TO_CART(state, payload) {
			console.log("addCart->", payload);
			let item = state.cart.find((i) => i.id === payload.id);

			if (item) {
				item.quantity++;
			} else {
				state.cart.push({ ...payload, quantity: 1 });
			}

			setLocalStorage(state.cart);
		},

		REMOVE_FROM_CART(state, payload) {
			console.log("removeCart->", payload);
			let item = state.cart.find((i) => i.id === payload.id);

			if (item) {
				if (item.quantity > 1) {
					item.quantity--;
				} else {
					state.cart = state.cart.filter((i) => i.id !== payload.id);
				}
			}

			setLocalStorage(state.cart);
		},

		UPDATE_CART(state) {
			const cart = localStorage.getItem("cart");
			if (cart) {
				state.cart = JSON.parse(cart);
			}
			//console.log("cart->", state.cart);
		},
		CLEAR_CART(state, payload) {
			const cart = localStorage.getItem("cart");
			if (cart) {
				state.cart = [];
			}

			setLocalStorage(state.cart);

			console.log("***", state.cart);
		},
		SET_CART_PRODUCT(state, payload) {
			state.CartProduct = payload;
		},

		SET_CART_COUNT(state, payload) {
			state.cartCount = payload;
		},

		SET_USER(state, payload) {
			localStorage.setItem("user", JSON.stringify(payload));
			axios.defaults.headers.common[
				"Authorization"
			] = `Bearer ${payload.token}`;
			state.user = payload;
		},
		LOGOUT() {
			localStorage.removeItem("user");
			location.reload();
		},
		IS_NEW_USER(state, isNewUser) {
			state.isNewUser = isNewUser;
		},

		SET_USERS_DATA(state, users) {
			state.users = users;
		},
	},

	actions: {
		async fetchProducts({ commit }) {
			try {
				const url = "https://fakestoreapi.com/products";
				const response = await axios.get(url);

				commit("SET_PRODUCTS", response.data);
				//console.log(response.data);
			} catch (error) {
				if (error.response) {
					// client received an error response (5xx, 4xx)
					console.log("Server Error:", error);
				} else if (error.request) {
					// client never received a response, or request never left
					console.log("Network Error:", error);
				} else {
					console.log("Client Error:", error);
				}
			}
		},

		fetchProductsPerPage({ commit }) {
			const url = "https://fakestoreapi.com/products?limit=10";
			axios
				.get(url)
				.then((response) => {
					commit("SET_PRODUCTS_PER_PAGE", response.data);
				})
				.catch((error) => {
					console.log(error);
				});
		},

		fetchCategories({ commit }) {
			const url = "https://fakestoreapi.com/products/categories";
			axios
				.get(url)
				.then((response) => {
					commit("SET_CATEGORIES", response.data);
				})
				.catch((error) => {
					if (error.response) {
						// client received an error response (5xx, 4xx)
						console.log("Server Error:", error);
					} else if (error.request) {
						// client never received a response, or request never left
						console.log("Network Error:", error);
					} else {
						console.log("Client Error:", error);
					}
				});
		},

		getUsers({ commit }) {
			axios
				.get("https://jsonplaceholder.typicode.com/users")
				.then((response) => {
					commit("SET_USERS_DATA", response.data);
				});
		},

		registerUser({ commit }, credentials) {
			const url = `https://fakestoreapi.com/users`;
			return axios
				.post(url, credentials)
				.then(({ data }) => {
					console.log("data from axios", data);
					//commit("SET_USER", data);
				})
				.catch((err) => {
					console.log("User Exist", err);
				});

			//console.log("credentials -> actions", credentials);
		},
		loginUser({ commit }, credentials) {
			const url = `https://fakestoreapi.com/auth/login`;
			axios.post(url, credentials).then(({ data }) => {
				commit("SET_USER", data);
			});
		},
		logoutUser({ commit }) {
			commit("LOGOUT");
		},
		isNewUser({ commit }, isNewUser) {
			commit("IS_NEW_USER", isNewUser);
		},
	},
});
