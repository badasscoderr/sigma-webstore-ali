const ctx = document.getElementById("totalSales").getContext("2d");

const labels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul'];
const data = {
	labels: labels,
	datasets: [
		{
			//label: "My First Dataset",
			data: [65, 59, 80, 81, 56, 55, 40],
			fill: true,
			borderColor: "#9d72ff",
			backgroundColor: "#523695",
			tension: 0.1,
		},
	],
};

const options = {
	scales: {
		x: {
			display: false,
		},
		y: {
			display: false,
		},
	},
	plugins: {
		legend: {
			display: false,
			labels: {
				color: 'rgb(255, 99, 132)'
			}
		}
	},
	responsive: true,
};

const config = {
	type: "line",
	data: data,
	options: options,
};

const totalSales = new Chart(ctx, config);
