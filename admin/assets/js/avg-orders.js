const avgOrdersCtx = document.getElementById("averargeOrder").getContext("2d");

const avgOrdersLabels = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul"];
const avgOrdersData = {
	labels: labels,
	datasets: [
		{
			data: [0, 10, 5, 2, 20, 30, 45],
			fill: true,
			borderColor: "#9d72ff",
			backgroundColor: "#523695",
			tension: 0.1,
		},
	],
};

const avgOrdersOptions = {
	scales: {
		x: {
			display: true,
		},
		y: {
			display: false,
		},
	},
	plugins: {
		legend: {
			display: false,
		},
	},
	responsive: true,
};

const avgOrdersConfig = {
	type: "bar",
	data: avgOrdersData,
	options: avgOrdersOptions,
};

const averargeOrders = new Chart(avgOrdersCtx, avgOrdersConfig);
