const Order = {
	template: /* html */ `
		<div class="nk-content-inner">
				<div class="nk-content-body">
						<div class="nk-block-head">
								<OrderHeader />
						</div><!-- .nk-block-head -->
						<div class="nk-block">
								<div class="invoice">
										<div class="invoice-wrap">
												<div class="invoice-brand text-center">
														<img src="./images/logo-dark.png" srcset="./images/logo-dark2x.png 2x" alt="">
												</div>
												<OrderTableHeader />
												<div class="invoice-bills">
														<div class="table-responsive">
																<OrderTable />
														</div>
												</div><!-- .invoice-bills -->
										</div><!-- .invoice-wrap -->
								</div><!-- .invoice -->
						</div><!-- .nk-block -->
				</div>
		</div>
	`,

	components: { OrderHeader, OrderTableHeader, OrderTable },

	computed: {},

	data() {},

	mounted() {},

	methods: {},
};
