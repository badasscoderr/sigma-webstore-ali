const Product = {
	template: /* html */ `
		<div class="nk-content-inner">
				<div class="nk-content-body">
						<ProductHeader />
						<div class="nk-block">
								<div class="card card-bordered">
										<div class="card-inner">
												<div class="row pb-5">
														<div class="col-lg-6">
																<div class="product-gallery mr-xl-1 mr-xxl-5">
																		<div class="slider-init">
																				<div class="slider-item rounded">
																						<img src="/admin/assets/images/lg-e.jpg" class="rounded w-100" alt="">
																				</div>
																		</div><!-- .slider-init -->

																</div><!-- .product-gallery -->
														</div><!-- .col -->
														<ProductInfo />
												</div><!-- .row -->
												<hr class="hr border-light">
												<ProductDesc />
										</div>
								</div>
						</div><!-- .nk-block -->

				</div>
		</div>
	`,

	components: { ProductHeader, ProductInfo, ProductDesc },

	computed: {},

	data() {},

	mounted() {},

	methods: {},
};
