const Products = {
	template: /* html */ `
		<!-- content @s -->
		<div class="nk-content-inner">
			<div class="nk-content-body">
				<ProductsHeader />
				<div class="nk-block">
					<ProductsTable />
					<Pagination />
				</div><!-- .nk-block -->
				<AddProductModal v-show="showAddProductModal" />
				<div class="toggle-overlay" v-show="showOverlay"></div>
			</div>
		</div>

    <!-- content @e -->
	`,

	components: { Pagination, ProductsTable, ProductsHeader },

	computed: {},

	data() {
		return {
			showAddProductModal: false,
			showOverlay: false,
		};
	},

	mounted() {},

	methods: {},
};
