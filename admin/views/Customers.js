const Customers = {
	template: /* html */ `
		<!-- content @s -->
		<div class="nk-content-inner">
			<div class="nk-content-body">
				<CustomersHeader />
				<div class="nk-block">
					<CustomersTable />
					<Pagination />
				</div><!-- .nk-block -->
				<!--<AddCustomerModal v-show="showAddOrderModal" />-->
				<div class="toggle-overlay" v-show="showOverlay"></div>
			</div>
		</div>

    <!-- content @e -->
	`,

	components: { Pagination, CustomersTable, CustomersHeader },

	computed: {},

	data() {
		return {
			showAddOrderModal: false,
			showOverlay: false,
		};
	},

	mounted() {},

	methods: {},
};
