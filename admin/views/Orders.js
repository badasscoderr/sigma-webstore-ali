const Orders = {
	template: /* html */ `
		<!-- content @s -->
		<div class="nk-content-inner">
			<div class="nk-content-body">
				<OrdersHeader />
				<div class="nk-block">
					<OrdersTable />
					<Pagination />
				</div><!-- .nk-block -->
				<AddOrderModal v-show="showAddOrderModal" />
				<div class="toggle-overlay" v-show="showOverlay"></div>
			</div>
		</div>

    <!-- content @e -->
	`,

	components: { Pagination, OrdersTable, OrdersHeader },

	computed: {},

	data() {
		return {
			showAddOrderModal: false,
			showOverlay: false,
		};
	},

	mounted() {},

	methods: {},
};
