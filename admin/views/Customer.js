const Customer = {
	template: /* html */ `
		<div class="nk-content-inner">
			<div class="nk-content-body">
					<CustomerHeader />
					<div class="nk-block">
							<div class="card">
									<div class="card-aside-wrap">
											<div class="card-content">
													<CustomerTabs />
													<div class="card-inner">
															<CustomerPersonalinfo />
															<CustomerExtraInfo />

															<div class="nk-divider divider md"></div>
															<AdminNote />
													</div><!-- .card-inner -->
											</div><!-- .card-content -->
											<CustomerModal />
									</div><!-- .card-aside-wrap -->
							</div><!-- .card -->
					</div><!-- .nk-block -->
			</div>
		</div>
	`,

	components: {
		CustomerHeader,
		CustomerModal,
		AdminNote,
		CustomerTabs,
		CustomerPersonalinfo,
		CustomerExtraInfo
	},

	computed: {},

	data() {},

	mounted() {},

	methods: {},
};
