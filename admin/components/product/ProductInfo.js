const ProductInfo = {
	template: /* html */ `
		<div class="col-lg-6">
				<div class="product-info mt-5 mr-xxl-5">
						<h4 class="product-price text-primary">$78.00 <small class="text-muted fs-14px">$98.00</small></h4>
						<h2 class="product-title">Classy Modern Smart watch</h2>
						<div class="product-rating">
								<ul class="rating">
										<li><em class="icon ni ni-star-fill"></em></li>
										<li><em class="icon ni ni-star-fill"></em></li>
										<li><em class="icon ni ni-star-fill"></em></li>
										<li><em class="icon ni ni-star-fill"></em></li>
										<li><em class="icon ni ni-star-half"></em></li>
								</ul>
								<div class="amount">(2 Reviews)</div>
						</div><!-- .product-rating -->
						<div class="product-excrept text-soft">
								<p class="lead">I must explain to you how all this mistaken idea of denoun cing ple praising pain was born and I will give you a complete account of the system, and expound the actual teaching.</p>
						</div>
						<div class="product-meta">
								<ul class="d-flex g-3 gx-5">
										<li>
												<div class="fs-14px text-muted">Type</div>
												<div class="fs-16px fw-bold text-secondary">Watch</div>
										</li>
										<li>
												<div class="fs-14px text-muted">Model Number</div>
												<div class="fs-16px fw-bold text-secondary">Forerunner 290XT</div>
										</li>
								</ul>
						</div>


				</div><!-- .product-info -->
		</div><!-- .col -->
	`,
};
