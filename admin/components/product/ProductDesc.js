const ProductDesc = {
	template: /* html */ `
		<div class="row g-gs flex-lg-row-reverse pt-5">

				<div class="col-lg-7">
						<div class="product-details entry mr-xxl-3">
								<h3>Product details of Comfy cushions</h3>
								<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Neque porro quisquam est, qui dolorem consectetur, adipisci velit.Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus.</p>
								<ul class="list list-sm list-checked">
										<li>Meets and/or exceeds performance standards.</li>
										<li>Liumbar support.</li>
										<li>Made of bonded teather and poiyurethane.</li>
										<li>Metal frame.</li>
										<li>Anatomically shaped cork-latex</li>
										<li>As attractively priced as you look attractive in one</li>
								</ul>
								<p>Unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae.</p>
								<h3>The best seats in the house</h3>
								<p>I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings. Unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae.</p>
						</div>
				</div><!-- .col -->
		</div><!-- .row -->
	`,
};
