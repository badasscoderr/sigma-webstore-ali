const ProductHeader = {
	template: /* html */ `
		<div class="nk-block-head nk-block-head-sm">
				<div class="nk-block-between g-3">
						<div class="nk-block-head-content">
								<h3 class="nk-block-title page-title">Product Details</h3>
								<div class="nk-block-des text-soft">
										<p>An example page for product details</p>
								</div>
						</div>
						<div class="nk-block-head-content">
								<router-link to="/admin/products" class="btn btn-outline-light bg-white d-none d-sm-inline-flex"><em class="icon ni ni-arrow-left"></em><span>Back</span></router-link>
								<router-link to="/admin/products" class="btn btn-icon btn-outline-light bg-white d-inline-flex d-sm-none"><em class="icon ni ni-arrow-left"></em></router-link>
						</div>
				</div>
		</div><!-- .nk-block-head -->
	`,
};
