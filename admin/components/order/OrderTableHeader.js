const OrderTableHeader = {
	template: /* html */ `
		<div class="invoice-head">
				<div class="invoice-contact">
						<span class="overline-title">Ordered by</span>
						<div class="invoice-contact-info">
								<h4 class="title">Gregory Ander son</h4>
								<ul class="list-plain">
										<li><em class="icon ni ni-map-pin-fill"></em><span>House #65, 4328 Marion Street<br>Newbury, VT 05051</span></li>
										<li><em class="icon ni ni-call-fill"></em><span>+012 8764 556</span></li>
								</ul>
						</div>
				</div>
				<div class="invoice-desc">
						<h3 class="title">Order</h3>
						<ul class="list-plain">
								<li class="invoice-id"><span>Order ID</span>:<span>66K5W3</span></li>
								<li class="invoice-date"><span>Date</span>:<span>8 Mar, 2022</span></li>
						</ul>
				</div>
		</div><!-- .invoice-head -->
	`,

	components: {},

	computed: {},

	data() {},

	mounted() {},

	methods: {},
};
