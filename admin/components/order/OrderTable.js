const OrderTable = {
	template: /* html */ `
		<table class="table table-striped">
				<thead>
						<tr>
								<th class="w-150px">Item ID</th>
								<th class="w-60">Description</th>
								<th>Price</th>
								<th>Qty</th>
								<th>Amount</th>
						</tr>
				</thead>
				<tbody>
						<tr>
								<td>24108054</td>
								<td>Dashlite - Conceptual App Dashboard - Regular License</td>
								<td>$40.00</td>
								<td>5</td>
								<td>$200.00</td>
						</tr>
						<tr>
								<td>24108054</td>
								<td>6 months premium support</td>
								<td>$25.00</td>
								<td>1</td>
								<td>$25.00</td>
						</tr>
						<tr>
								<td>23604094</td>
								<td>Invest Management Dashboard - Regular License</td>
								<td>$131.25</td>
								<td>1</td>
								<td>$131.25</td>
						</tr>
						<tr>
								<td>23604094</td>
								<td>6 months premium support</td>
								<td>$78.75</td>
								<td>1</td>
								<td>$78.75</td>
						</tr>
				</tbody>
				<tfoot>
						<tr>
								<td colspan="2"></td>
								<td colspan="2">Subtotal</td>
								<td>$435.00</td>
						</tr>
						<tr>
								<td colspan="2"></td>
								<td colspan="2">Processing fee</td>
								<td>$10.00</td>
						</tr>
						<tr>
								<td colspan="2"></td>
								<td colspan="2">TAX</td>
								<td>$43.50</td>
						</tr>
						<tr>
								<td colspan="2"></td>
								<td colspan="2">Grand Total</td>
								<td>$478.50</td>
						</tr>
				</tfoot>
		</table>
	`,

	components: {},

	computed: {},

	data() {},

	mounted() {},

	methods: {},
};
