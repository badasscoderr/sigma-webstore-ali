const OrderHeader = {
	template: /* html */ `
		<div class="nk-block-between g-3">
				<div class="nk-block-head-content">
						<h3 class="nk-block-title page-title">Invoice <strong class="text-primary small">#746F5K2</strong></h3>
						<div class="nk-block-des text-soft">
								<ul class="list-inline">
										<li>Ordered At: <span class="text-base">8 Mar, 2022 01:02 PM</span></li>
								</ul>
						</div>
				</div>
				<div class="nk-block-head-content">
						<router-link to="/admin/orders" class="btn btn-outline-light bg-white d-none d-sm-inline-flex"><em class="icon ni ni-arrow-left"></em><span>Back</span></router-link>
						<router-link to="/admin/orders" class="btn btn-icon btn-outline-light bg-white d-inline-flex d-sm-none"><em class="icon ni ni-arrow-left"></em></router-link>
				</div>
		</div>
	`,

	components: {},

	computed: {},

	data() {},

	mounted() {},

	methods: {},
};
