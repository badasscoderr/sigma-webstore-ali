const Navigation = {
	template: /* html */ `
		<div class="nk-header nk-header-fixed is-light">
			<div class="container-fluid">
				<div class="nk-header-wrap">
					<div class="nk-menu-trigger d-xl-none ml-n1">
						<a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
					</div>
					<div class="nk-header-brand d-xl-none">
						<a href="html/index.html" class="logo-link">
							<img class="logo-dark logo-img" src="/admin/assets/images/logo-dark2x.png" alt="logo-dark">
						</a>
					</div><!-- .nk-header-brand -->

					<div class="nk-header-tools">
						<ul class="nk-quick-nav">
							<!-- Notifications --->
							<li class="dropdown notification-dropdown">
									<a href="#" class="dropdown-toggle nk-quick-nav-icon" data-toggle="dropdown">
										<div class="icon-status icon-status-info"><em class="icon ni ni-bell"></em></div>
									</a>
									<NotifDropdown v-show="showNotifDropdown" />
							</li>
							<!-- User --->
							<li class="dropdown user-dropdown">
								<a href="#" class="dropdown-toggle mr-n1" data-toggle="dropdown">
										<div class="user-toggle">
												<div class="user-avatar sm">
														<em class="icon ni ni-user-alt"></em>
												</div>
												<div class="user-info d-none d-xl-block">
														<div class="user-status user-status-active">Administator</div>
														<div class="user-name dropdown-indicator">Abu Bin Ishityak</div>
												</div>
										</div>
								</a>
								<UserDropdown v-show="showUserDropdown" />
							</li>
						</ul>
					</div>
				</div><!-- .nk-header-wrap -->
			</div><!-- .container-fliud -->
    </div>
	`,

	data() {
		return {
			showNotifDropdown: false,
			showUserDropdown: false,
		};
	},
};


