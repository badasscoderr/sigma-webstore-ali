const Pagination = {
	template: /* html */ `
		<div class="card">
			<div class="card-inner">
				<div class="nk-block-between-md g-3">
					<div class="g">
						<ul class="pagination justify-content-center justify-content-md-start">
							<li class="page-item"><a class="page-link" href="#"><em class="icon ni ni-chevrons-left"></em></a></li>
							<li class="page-item"><a class="page-link" href="#">1</a></li>
							<li class="page-item"><a class="page-link" href="#">2</a></li>
							<li class="page-item"><span class="page-link"><em class="icon ni ni-more-h"></em></span></li>
							<li class="page-item"><a class="page-link" href="#">6</a></li>
							<li class="page-item"><a class="page-link" href="#">7</a></li>
							<li class="page-item"><a class="page-link" href="#"><em class="icon ni ni-chevrons-right"></em></a></li>
						</ul><!-- .pagination -->
					</div>
					<div class="g">
						<div
							class="pagination-goto
							d-flex
							justify-content-center
							justify-content-md-start
							gx-3"
						>
							<div>Page</div>
							<div>
								<span
									class="select2
									select2-container
									select2-container--default"
									style="width: 41px"
								>
									<span class="selection">
										<span
											class="pl-2 select2-selection
											select2-selection--single"
										>
											<span class="">1</span>
												<span class="select2-selection__arrow"></span>
													<select class="form-select">
														<option value="page-1">1</option>
														<option value="page-2">2</option>
														<option value="page-4">4</option>
														<option value="page-5">5</option>
														<option value="page-6">6</option>
														<option value="page-7">7</option>
														<option value="page-8">8</option>
														<option value="page-9">9</option>
														<option value="page-10">10</option>
														<option value="page-11">11</option>
														<option value="page-12">12</option>
														<option value="page-13">13</option>
														<option value="page-14">14</option>
														<option value="page-15">15</option>
														<option value="page-16">16</option>
														<option value="page-17">17</option>
														<option value="page-18">18</option>
														<option value="page-19">19</option>
														<option value="page-20">20</option>
													</select>
											</span>
										</span>
									</span>
								</span>
							</div>
							<div>OF 102</div>
						</div>
					</div><!-- .pagination-goto -->
				</div><!-- .nk-block-between -->
			</div>
		</div>
	`,

	components: {},

	computed: {},

	data() {},

	mounted() {},

	methods: {},
};
