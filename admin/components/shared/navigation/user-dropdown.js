const UserDropdown = {
	template: /* html */ `
		<div class="dropdown-menu dropdown-menu-md dropdown-menu-right show">
			<div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
					<div class="user-card">
							<div class="user-avatar">
									<span>AB</span>
							</div>
							<div class="user-info">
									<span class="lead-text">Abu Bin Ishtiyak</span>
									<span class="sub-text">info@softnio.com</span>
							</div>
					</div>
			</div>
			<div class="dropdown-inner">
					<ul class="link-list">
							<li><a href="html/ecommerce/user-profile.html"><em class="icon ni ni-user-alt"></em><span>View Profile</span></a></li>
							<li><a href="html/ecommerce/user-profile.html"><em class="icon ni ni-setting-alt"></em><span>Account Setting</span></a></li>
							<li><a href="html/ecommerce/user-profile.html"><em class="icon ni ni-activity-alt"></em><span>Login Activity</span></a></li>
							<li><a class="dark-switch" href="#"><em class="icon ni ni-moon"></em><span>Dark Mode</span></a></li>
					</ul>
			</div>
			<div class="dropdown-inner">
					<ul class="link-list">
							<li><a href="#"><em class="icon ni ni-signout"></em><span>Sign out</span></a></li>
					</ul>
			</div>
		</div>
	`,

	components: {},

	computed: {},

	data() {},

	mounted() {},

	methods: {},
};
