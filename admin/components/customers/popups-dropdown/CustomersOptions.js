const CustomersOptions = {
	template: /* html */ `
		<div class="dropdown-menu dropdown-menu-right show">
			<ul class="link-list-opt no-bdr">
					<li><a href="#"><em class="icon ni ni-mail"></em><span>Send Email to All</span></a></li>
					<li><a href="#"><em class="icon ni ni-na"></em><span>Suspend Selected</span></a></li>
					<li><a href="#"><em class="icon ni ni-trash"></em><span>Remove Seleted</span></a></li>
					<li><a href="#"><em class="icon ni ni-shield-star"></em><span>Reset Password</span></a></li>
			</ul>
		</div>
	`,

	components: {},

	computed: {},

	data() {},

	mounted() {},

	methods: {},
};
