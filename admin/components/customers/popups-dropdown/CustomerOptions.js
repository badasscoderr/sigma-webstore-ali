const CustomerOptions = {
	template: /* html */ `
		<div class="dropdown-menu dropdown-menu-right show">
			<ul class="link-list-opt no-bdr">
				<li><a href="html/ecommerce/customer-details.html"><em class="icon ni ni-eye"></em><span>View Details</span></a></li>
				<li><a href="#"><em class="icon ni ni-repeat"></em><span>Orders</span></a></li>
				<li><a href="#"><em class="icon ni ni-activity-round"></em><span>Activities</span></a></li>
				<li class="divider"></li>
				<li><a href="#"><em class="icon ni ni-shield-star"></em><span>Reset Pass</span></a></li>
				<li><a href="#"><em class="icon ni ni-na"></em><span>Suspend</span></a></li>
			</ul>
		</div>
	`,

	components: {},

	computed: {},

	data() {},

	mounted() {},

	methods: {},
};
