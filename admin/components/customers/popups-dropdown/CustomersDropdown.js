const CustomersDropdown = {
	template: /* html */ `
		<div class="dropdown-menu dropdown-menu-right show" style="position: absolute;
    top: 55%;" x-placement="bottom-end">
			<ul class="link-list-opt no-bdr">
				<li><a href="#"><span>Actived</span></a></li>
				<li><a href="#"><span>Inactived</span></a></li>
				<li><a href="#"><span>Blocked</span></a></li>
			</ul>
		</div>
	`,

	components: {},

	computed: {},

	data() {},

	mounted() {},

	methods: {},
};
