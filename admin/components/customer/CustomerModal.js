const CustomerModal = {
	template: /* html */ `
		<div class="card-aside card-aside-right user-aside toggle-slide toggle-slide-right toggle-break-xxl" data-content="userAside" data-toggle-screen="xxl" data-toggle-overlay="true" data-toggle-body="true">
                                                <div class="card-inner-group" data-simplebar>
                                                    <div class="card-inner">
                                                        <div class="user-card user-card-s2">
                                                            <div class="user-avatar lg bg-primary">
                                                                <span>AB</span>
                                                            </div>
                                                            <div class="user-info">
                                                                <div class="badge badge-outline-light badge-pill ucap">Customer</div>
                                                                <h5>Abu Bin Ishtiyak</h5>
                                                                <span class="sub-text">info@softnio.com</span>
                                                            </div>
                                                        </div>
                                                    </div><!-- .card-inner -->
                                                    <div class="card-inner card-inner-sm">
                                                        <ul class="btn-toolbar justify-center gx-1">
                                                            <li><a href="#" class="btn btn-trigger btn-icon"><em class="icon ni ni-shield-off"></em></a></li>
                                                            <li><a href="#" class="btn btn-trigger btn-icon"><em class="icon ni ni-mail"></em></a></li>
                                                            <li><a href="#" class="btn btn-trigger btn-icon"><em class="icon ni ni-download-cloud"></em></a></li>
                                                            <li><a href="#" class="btn btn-trigger btn-icon"><em class="icon ni ni-bookmark"></em></a></li>
                                                            <li><a href="#" class="btn btn-trigger btn-icon text-danger"><em class="icon ni ni-na"></em></a></li>
                                                        </ul>
                                                    </div><!-- .card-inner -->
                                                    <div class="card-inner">
                                                        <div class="overline-title-alt mb-2">Ordered Amount</div>
                                                        <div class="profile-balance">
                                                            <div class="profile-balance-group gx-4">
                                                                <div class="profile-balance-sub">
                                                                    <div class="profile-balance-amount">
                                                                        <div class="number">2,556.57 <small class="currency currency-usd">USD</small></div>
                                                                    </div>
                                                                    <div class="profile-balance-subtitle">Complete Order</div>
                                                                </div>
                                                                <div class="profile-balance-sub">
                                                                    <span class="profile-balance-plus text-soft"><em class="icon ni ni-plus"></em></span>
                                                                    <div class="profile-balance-amount">
                                                                        <div class="number">1,143.76</div>
                                                                    </div>
                                                                    <div class="profile-balance-subtitle">Pending Order</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><!-- .card-inner -->
                                                    <div class="card-inner">
                                                        <div class="row text-center">
                                                            <div class="col-4">
                                                                <div class="profile-stats">
                                                                    <span class="amount">23</span>
                                                                    <span class="sub-text">Total Order</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="profile-stats">
                                                                    <span class="amount">20</span>
                                                                    <span class="sub-text">Complete</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="profile-stats">
                                                                    <span class="amount">3</span>
                                                                    <span class="sub-text">Progress</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><!-- .card-inner -->
                                                    <div class="card-inner">
                                                        <h6 class="overline-title-alt mb-2">Additional</h6>
                                                        <div class="row g-3">
                                                            <div class="col-6">
                                                                <span class="sub-text">User ID:</span>
                                                                <span>UD003054</span>
                                                            </div>
                                                            <div class="col-6">
                                                                <span class="sub-text">Last Login:</span>
                                                                <span>15 Jun, 2020 01:02 PM</span>
                                                            </div>
                                                            <div class="col-6">
                                                                <span class="sub-text">Email Verify:</span>
                                                                <span class="lead-text text-success">Verified</span>
                                                            </div>
                                                            <div class="col-6">
                                                                <span class="sub-text">Register At:</span>
                                                                <span>Nov 24, 2019</span>
                                                            </div>
                                                        </div>
                                                    </div><!-- .card-inner -->
                                                    <div class="card-inner">
                                                        <h6 class="overline-title-alt mb-3">Groups</h6>
                                                        <ul class="g-1">
                                                            <li class="btn-group">
                                                                <a class="btn btn-xs btn-light btn-dim" href="#">Customer</a>
                                                                <a class="btn btn-xs btn-icon btn-light btn-dim" href="#"><em class="icon ni ni-cross"></em></a>
                                                            </li>
                                                            <li class="btn-group">
                                                                <a class="btn btn-xs btn-light btn-dim" href="#">another tag</a>
                                                                <a class="btn btn-xs btn-icon btn-light btn-dim" href="#"><em class="icon ni ni-cross"></em></a>
                                                            </li>
                                                        </ul>
                                                    </div><!-- .card-inner -->
                                                </div><!-- .card-inner -->
                                            </div><!-- .card-aside -->
	`,

	components: {},

	computed: {},

	data() {},

	mounted() {},

	methods: {},
};
