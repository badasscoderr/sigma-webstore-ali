const CustomerTabs = {
	template: /* html */ `
		<ul class="nav nav-tabs nav-tabs-mb-icon nav-tabs-card">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#"><em class="icon ni ni-user-circle"></em><span>Personal</span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#"><em class="icon ni ni-repeat"></em><span>Orders</span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#"><em class="icon ni ni-bell"></em><span>Notifications</span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#"><em class="icon ni ni-activity"></em><span>Activities</span></a>
                                                    </li>
                                                    <li class="nav-item nav-item-trigger d-xxl-none">
                                                        <a href="#" class="toggle btn btn-icon btn-trigger" data-target="userAside"><em class="icon ni ni-user-list-fill"></em></a>
                                                    </li>
                                                </ul><!-- .nav-tabs -->
	`,

	components: {},

	computed: {},

	data() {},

	mounted() {},

	methods: {},
};
