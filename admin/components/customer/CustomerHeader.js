const CustomerHeader = {
	template: /* html */ `
		<div class="nk-block-head nk-block-head-sm">
			<div class="nk-block-between g-3">
					<div class="nk-block-head-content">
							<h3 class="nk-block-title page-title">Customer / <strong class="text-primary small">Abu Bin Ishtiyak</strong></h3>
					</div>
					<div class="nk-block-head-content">
							<router-link to="/admin/customers" class="btn btn-outline-light bg-white d-none d-sm-inline-flex"><em class="icon ni ni-arrow-left"></em><span>Back</span></router-link>
							<a href="html/ecommerce/customers.html" class="btn btn-icon btn-outline-light bg-white d-inline-flex d-sm-none"><em class="icon ni ni-arrow-left"></em></a>
					</div>
			</div>
		</div><!-- .nk-block-head -->
	`,

	components: {},

	computed: {},

	data() {},

	mounted() {},

	methods: {},
};
