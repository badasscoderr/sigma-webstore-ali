const AdminNote = {
	template: /* html */ `
		<div class="nk-block">
                                                        <div class="nk-block-head nk-block-head-sm nk-block-between">
                                                            <h5 class="title">Admin Note</h5>
                                                            <a href="#" class="link link-sm">+ Add Note</a>
                                                        </div><!-- .nk-block-head -->
                                                        <div class="bq-note">
                                                            <div class="bq-note-item">
                                                                <div class="bq-note-text">
                                                                    <p>Aproin at metus et dolor tincidunt feugiat eu id quam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean sollicitudin non nunc vel pharetra. </p>
                                                                </div>
                                                                <div class="bq-note-meta">
                                                                    <span class="bq-note-added">Added on <span class="date">November 18, 2019</span> at <span class="time">5:34 PM</span></span>
                                                                    <span class="bq-note-sep sep">|</span>
                                                                    <span class="bq-note-by">By <span>Softnio</span></span>
                                                                    <a href="#" class="link link-sm link-danger">Delete Note</a>
                                                                </div>
                                                            </div><!-- .bq-note-item -->
                                                            <div class="bq-note-item">
                                                                <div class="bq-note-text">
                                                                    <p>Aproin at metus et dolor tincidunt feugiat eu id quam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean sollicitudin non nunc vel pharetra. </p>
                                                                </div>
                                                                <div class="bq-note-meta">
                                                                    <span class="bq-note-added">Added on <span class="date">November 18, 2019</span> at <span class="time">5:34 PM</span></span>
                                                                    <span class="bq-note-sep sep">|</span>
                                                                    <span class="bq-note-by">By <span>Softnio</span></span>
                                                                    <a href="#" class="link link-sm link-danger">Delete Note</a>
                                                                </div>
                                                            </div><!-- .bq-note-item -->
                                                        </div><!-- .bq-note -->
                                                    </div><!-- .nk-block -->
	`,

	components: {},

	computed: {},

	data() {},

	mounted() {},

	methods: {},
};
