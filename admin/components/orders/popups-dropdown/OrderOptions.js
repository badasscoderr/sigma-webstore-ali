const OrderOptions = {
	template: /* html */ `
		<div class="dropdown-menu dropdown-menu-right show">
			<ul class="link-list-opt no-bdr">
					<li><a href="#"><em class="icon ni ni-edit"></em><span>Update Status</span></a></li>
					<li><a href="#"><em class="icon ni ni-truck"></em><span>Mark as Delivered</span></a></li>
					<li><a href="#"><em class="icon ni ni-money"></em><span>Mark as Paid</span></a></li>
					<li><a href="#"><em class="icon ni ni-report-profit"></em><span>Send Invoice</span></a></li>
					<li><a href="#"><em class="icon ni ni-trash"></em><span>Remove Orders</span></a></li>
			</ul>
		</div>
	`,

	components: {},

	computed: {},

	data() {},

	mounted() {},

	methods: {},
};
