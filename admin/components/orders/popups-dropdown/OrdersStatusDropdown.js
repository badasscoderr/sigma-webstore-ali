const OrdersStatusDropdown = {
	template: /* html */ `
		<div class="dropdown-menu dropdown-menu-right show" style="position: absolute;
    top: 55%;" x-placement="bottom-end">
			<ul class="link-list-opt no-bdr">
				<li><a href="#"><span>Hold</span></a></li>
				<li><a href="#"><span>Delivered</span></a></li>
				<li><a href="#"><span>Rejected</span></a></li>
			</ul>
		</div>
	`,

	components: {},

	computed: {},

	data() {},

	mounted() {},

	methods: {},
};
