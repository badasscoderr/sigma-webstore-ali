const OrdersTable = {
	template: /* html */ `
		<div class="nk-tb-list is-separate is-medium mb-3">
				<div class="nk-tb-item nk-tb-head">
						<div class="nk-tb-col nk-tb-col-check">
								<div class="custom-control custom-control-sm custom-checkbox notext">
										<input type="checkbox" class="custom-control-input" id="oid">
										<label class="custom-control-label" for="oid"></label>
								</div>
						</div>
						<div class="nk-tb-col"><span>Order</span></div>
						<div class="nk-tb-col tb-col-md"><span>Date</span></div>
						<div class="nk-tb-col"><span class="d-none d-mb-block">Status</span></div>
						<div class="nk-tb-col tb-col-sm"><span>Customer</span></div>
						<div class="nk-tb-col tb-col-md"><span>Purchased</span></div>
						<div class="nk-tb-col"><span>Total</span></div>
						<div class="nk-tb-col nk-tb-col-tools">
								<ul class="nk-tb-actions gx-1 my-n1">
										<li>
												<div class="drodown">
														<a href="#" class="dropdown-toggle btn btn-icon btn-trigger mr-n1" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
														<OrdersOptions v-show="showOrdersOptions"/>
												</div>
										</li>
								</ul>
						</div>
				</div><!-- .nk-tb-item -->
				<div class="nk-tb-item">
						<div class="nk-tb-col nk-tb-col-check">
								<div class="custom-control custom-control-sm custom-checkbox notext">
										<input type="checkbox" class="custom-control-input" id="oid01">
										<label class="custom-control-label" for="oid01"></label>
								</div>
						</div>
						<div class="nk-tb-col">

							<span class="tb-lead">
								<router-link to="/admin/orders/order">
									#95954
								</router-link>
							</span>
						</div>
						<div class="nk-tb-col tb-col-md">
								<span class="tb-sub">Jun 4, 2020</span>
						</div>
						<div class="nk-tb-col">
								<span class="dot bg-warning d-mb-none"></span>
								<span class="badge badge-sm badge-dot has-bg badge-warning d-none d-mb-inline-flex">On Hold</span>
						</div>
						<div class="nk-tb-col tb-col-sm">
								<span class="tb-sub">Arnold Armstrong</span>
						</div>
						<div class="nk-tb-col tb-col-md">
								<span class="tb-sub text-primary">3 Items</span>
						</div>
						<div class="nk-tb-col">
								<span class="tb-lead">$ 249.75</span>
						</div>
						<div class="nk-tb-col nk-tb-col-tools">
								<ul class="nk-tb-actions gx-1">
										<li class="nk-tb-action-hidden"><a href="#" class="btn btn-icon btn-trigger btn-tooltip" title="Mark as Delivered" data-toggle="dropdown">
														<em class="icon ni ni-truck"></em></a></li>
										<li class="nk-tb-action-hidden"><router-link to="/admin/orders/order" class="btn btn-icon btn-trigger btn-tooltip" title="View Order" data-toggle="dropdown">
														<em class="icon ni ni-eye"></em></router-link></li>
										<li>
												<div class="drodown mr-n1">
														<a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>

														<OrderOptions v-show="showOrderOptions"/>
												</div>
										</li>
								</ul>
						</div>
				</div><!-- .nk-tb-item -->

		</div><!-- .nk-tb-list -->
	`,

	components: {},

	computed: {},

	data() {
		return {
			showOrdersOptions: false,
			showOrderOptions: false,
		};
	},

	mounted() {},

	methods: {},
};
