const OrdersHeader = {
	template: /* html */ `
		<div class="nk-block-head nk-block-head-sm">
			<div class="nk-block-between">
				<div class="nk-block-head-content">
					<h3 class="nk-block-title page-title">Orders</h3>
				</div><!-- .nk-block-head-content -->
				<div class="nk-block-head-content">
					<div class="toggle-wrap nk-block-tools-toggle">
						<a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1"></em></a>
						<div class="toggle-expand-content">
							<ul class="nk-block-tools g-3">
								<li>
									<div class="form-control-wrap">
										<div class="form-icon form-icon-right">
											<em class="icon ni ni-search"></em>
										</div>
										<input type="text" class="form-control" id="default-04" placeholder="Quick search by id">
									</div>
								</li>
								<li>
									<div class="drodown">
										<router-link to=""
											class="dropdown-toggle
											dropdown-indicator
											btn btn-outline-light
											btn-white"
										>
											Status
										</router-link>
										<OrdersStatusDropdown v-show="showStatusDropdown" />
									</div>
								</li>
								<!--<li class="nk-block-tools-opt">
									<a href="#" class="toggle btn btn-icon btn-primary d-md-none">
										<em class="icon ni ni-plus"></em>
									</a>
									<a href="#" class="toggle btn btn-primary d-none d-md-inline-flex">
										<em class="icon ni ni-plus"></em>
										<span>Add Order</span>
									</a>
								</li>-->
							</ul>
						</div>
					</div>
				</div><!-- .nk-block-head-content -->
			</div><!-- .nk-block-between -->
		</div><!-- .nk-block-head -->
	`,

	components: {},

	computed: {},

	data() {
		return {
			showStatusDropdown: false,
		};
	},

	mounted() {},

	methods: {},
};
