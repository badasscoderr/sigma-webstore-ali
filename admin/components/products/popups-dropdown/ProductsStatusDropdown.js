const ProductsStatusDropdown = {
	template: /* html */ `
		<div class="dropdown-menu dropdown-menu-right show" style="position: absolute;
    top: 55%;" x-placement="bottom-end">
			<ul class="link-list-opt no-bdr">
				<li><a href="#"><span>New Items</span></a></li>
				<li><a href="#"><span>Featured</span></a></li>
				<li><a href="#"><span>Out of Stock</span></a></li>
			</ul>
		</div>
	`,

	components: {},

	computed: {},

	data() {},

	mounted() {},

	methods: {},
};
