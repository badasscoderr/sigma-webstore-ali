const ProductsOptions = {
	template: /* html */ `
		<div class="dropdown-menu dropdown-menu-right show">
			<ul class="link-list-opt no-bdr">
					<li><a href="#"><em class="icon ni ni-edit"></em><span>Edit Selected</span></a></li>
					<li><a href="#"><em class="icon ni ni-trash"></em><span>Remove Selected</span></a></li>
					<li><a href="#"><em class="icon ni ni-bar-c"></em><span>Update Stock</span></a></li>
					<li><a href="#"><em class="icon ni ni-invest"></em><span>Update Price</span></a></li>
			</ul>
		</div>
	`,

	components: {},

	computed: {},

	data() {},

	mounted() {},

	methods: {},
};
