const ProductOptions = {
	template: /* html */ `
		<div class="dropdown-menu dropdown-menu-right show">
			<ul class="link-list-opt no-bdr">
					<li><a href="#"><em class="icon ni ni-edit"></em><span>Edit Product</span></a></li>
					<li><a href="#"><em class="icon ni ni-eye"></em><span>View Product</span></a></li>
					<li><a href="#"><em class="icon ni ni-activity-round"></em><span>Product Orders</span></a></li>
					<li><a href="#"><em class="icon ni ni-trash"></em><span>Remove Product</span></a></li>
			</ul>
		</div>
	`,

	components: {},

	computed: {},

	data() {},

	mounted() {},

	methods: {},
};
