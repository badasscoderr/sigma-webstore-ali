const ProductsTable = {
	template: /* html */ `
		<div class="nk-tb-list is-separate mb-3">
				<div class="nk-tb-item nk-tb-head">
						<div class="nk-tb-col nk-tb-col-check">
								<div class="custom-control custom-control-sm custom-checkbox notext">
										<input type="checkbox" class="custom-control-input" id="pid">
										<label class="custom-control-label" for="pid"></label>
								</div>
						</div>
						<div class="nk-tb-col tb-col-sm"><span>Name</span></div>
						<div class="nk-tb-col"><span>SKU</span></div>
						<div class="nk-tb-col"><span>Price</span></div>
						<div class="nk-tb-col"><span>Stock</span></div>
						<div class="nk-tb-col tb-col-md"><span>Category</span></div>
						<div class="nk-tb-col tb-col-md"><em class="tb-asterisk icon ni ni-star-round"></em></div>
						<div class="nk-tb-col nk-tb-col-tools">
								<ul class="nk-tb-actions gx-1 my-n1">
										<li class="mr-n1">
												<div class="dropdown">
														<a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
														<ProductsOptions v-show="showProductsOptions" />
												</div>
										</li>
								</ul>
						</div>
				</div><!-- .nk-tb-item -->
				<div class="nk-tb-item">
						<div class="nk-tb-col nk-tb-col-check">
								<div class="custom-control custom-control-sm custom-checkbox notext">
										<input type="checkbox" class="custom-control-input" id="pid1">
										<label class="custom-control-label" for="pid1"></label>
								</div>
						</div>
						<div class="nk-tb-col tb-col-sm">
							<router-link to="/admin/products/product">
								<span class="tb-product">
									<img src="/admin/assets/images/product.png" alt="" class="thumb">
									<span class="title">Pink Fitness Tracker</span>
								</span>
							</router-link>
						</div>
						<div class="nk-tb-col">
								<span class="tb-sub">UY3749</span>
						</div>
						<div class="nk-tb-col">
								<span class="tb-lead">$ 99.49</span>
						</div>
						<div class="nk-tb-col">
								<span class="tb-sub">49</span>
						</div>
						<div class="nk-tb-col tb-col-md">
								<span class="tb-sub">Fitbit, Tracker</span>
						</div>
						<div class="nk-tb-col tb-col-md">
								<div class="asterisk tb-asterisk">
										<a href="#"><em class="asterisk-off icon ni ni-star"></em><em class="asterisk-on icon ni ni-star-fill"></em></a>
								</div>
						</div>
						<div class="nk-tb-col nk-tb-col-tools">
								<ul class="nk-tb-actions gx-1 my-n1">
										<li class="mr-n1">
												<div class="dropdown">
														<a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
														<ProductOptions v-show="showProductOptions"/>

												</div>
										</li>
								</ul>
						</div>
					</div>
		</div><!-- .nk-tb-list -->
	`,

	components: {},

	computed: {},

	data() {
		return {
			showProductOptions: false,
			showProductsOptions: false,
		};
	},

	mounted() {},

	methods: {},
};
