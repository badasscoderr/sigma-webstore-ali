const app = Vue.createApp(App);

app.directive();

app.component("NotifDropdown", NotifDropdown);
app.component("UserDropdown", UserDropdown);

app.component("ProductsStatusDropdown", ProductsStatusDropdown);
app.component("AddProductModal", AddProductModal);
app.component("ProductsOptions", ProductsOptions);
app.component("ProductOptions", ProductOptions);

app.component("OrdersStatusDropdown", OrdersStatusDropdown);
app.component("AddOrderModal", AddOrderModal);
app.component("OrdersOptions", OrdersOptions);
app.component("OrderOptions", OrderOptions);

app.component("CustomersDropdown", CustomersDropdown);
app.component("AddCustomerModal", AddCustomerModal);
app.component("CustomersOptions", CustomersOptions);
app.component("CustomerOptions", CustomerOptions);

app.use(router);
app.mount('#app');
