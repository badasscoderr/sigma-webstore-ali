const App = {
	template: /* html */ `
		<main class="nk-body bg-lighter npc-general has-sidebar ">
    	<div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main">
					<Aside />
					<div class="nk-wrap ">
						<!-- main header @s -->

						<Navigation />


						<div class="nk-content ">
              <div class="container-fluid">
						  	<router-view />
							</div>
						</div>
					</div>
				</div>
			</div>
		<main>
	`,

	components: { Navigation, Aside },

	computed: {},

	data() {},

	mounted() {},

	methods: {},
};



