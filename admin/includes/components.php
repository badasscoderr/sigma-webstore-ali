 <!-- Components Registration ----------------->
 <!-- Shared Components Registration --->
 <script src="/admin/components/shared/Navigation.js"></script>
 <script src="/admin/components/shared/navigation/notif-dropdown.js"></script>
 <script src="/admin/components/shared/navigation/user-dropdown.js"></script>
 <script src="/admin/components/shared/Aside.js"></script>
 <script src="/admin/components/shared/Pagination.js"></script>

 <!-- Products Components Registration --->
 <script src="/admin/components/products/ProductsTable.js"></script>
 <script src="/admin/components/products/ProductsHeader.js"></script>
 <script src="/admin/components/products/popups-dropdown/ProductsStatusDropdown.js"></script>
 <script src="/admin/components/products/popups-dropdown/AddProductModal.js"></script>
 <script src="/admin/components/products/popups-dropdown/ProductsOptions.js"></script>
 <script src="/admin/components/products/popups-dropdown/ProductOptions.js"></script>

 <!-- Orders Components Registration --->
 <script src="/admin/components/orders/OrdersTable.js"></script>
 <script src="/admin/components/orders/OrdersHeader.js"></script>
 <script src="/admin/components/orders/popups-dropdown/OrdersStatusDropdown.js"></script>
 <script src="/admin/components/orders/popups-dropdown/AddOrderModal.js"></script>
 <script src="/admin/components/orders/popups-dropdown/OrdersOptions.js"></script>
 <script src="/admin/components/orders/popups-dropdown/OrderOptions.js"></script>

 <!-- Customers Components Registration --->
 <script src="/admin/components/customers/CustomersTable.js"></script>
 <script src="/admin/components/customers/CustomersHeader.js"></script>
 <script src="/admin/components/customers/popups-dropdown/CustomersDropdown.js"></script>
 <script src="/admin/components/customers/popups-dropdown/CustomersOptions.js"></script>
 <script src="/admin/components/customers/popups-dropdown/CustomerOptions.js"></script>
 <script src="/admin/components/customers/popups-dropdown/AddCustomerModal.js"></script>

 <!-- Customer Components Registration --->
 <script src="/admin/components/customer/CustomerHeader.js"></script>
 <script src="/admin/components/customer/CustomerModal.js"></script>
 <script src="/admin/components/customer/AdminNote.js"></script>
 <script src="/admin/components/customer/CustomerTabs.js"></script>
 <script src="/admin/components/customer/CustomerPersonalinfo.js"></script>
 <script src="/admin/components/customer/CustomerExtraInfo.js"></script>

 <!-- Order Components Registration --->
 <script src="/admin/components/order/OrderTableHeader.js"></script>
 <script src="/admin/components/order/OrderHeader.js"></script>
 <script src="/admin/components/order/OrderTable.js"></script>

 <!-- Product Components Registration --->
 <script src="/admin/components/product/ProductHeader.js"></script>
 <script src="/admin/components/product/ProductInfo.js"></script>
 <script src="/admin/components/product/ProductDesc.js"></script>


 <!-- Views Registration --->
 <script src="/admin/views/Dashboard.js"></script>
 <script src="/admin/views/Products.js"></script>
 <script src="/admin/views/Orders.js"></script>
 <script src="/admin/views/Order.js"></script>
 <script src="/admin/views/Customers.js"></script>
 <script src="/admin/views/Customer.js"></script>
 <script src="/admin/views/Product.js"></script>
 <script src="/admin/views/Blank.js"></script>


 <script>
	 console.log('\n>> ', Blank); // eslint-disable-line
	 console.log('\n>> ', ProductOptions); // eslint-disable-line

 </script>
