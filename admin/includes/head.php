<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Dashboard - Sigma</title>

	<!-- Fav Icon  -->
	<link rel="shortcut icon" href="./images/favicon.png">

	<!-- Vue cdns --->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/3.2.26/vue.global.prod.min.js" integrity="sha512-yY2w8QVShzoLAachKPHtZRjXZeQOi9rQ2dYEYLf+lelt+TvZVOm/AlqVX6xFrjiy6wKDxgqvT1RL3BjxPdq/UA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/vuex/4.0.2/vuex.global.prod.min.js"
	integrity="sha512-6qTR+Lfgwi+S5KFpSBVNXu/OXZszhGyc2Fw7a2yM07+k5EUkOQwcPdTr+FmyLb5A9MbQaQ5vhpoNIIj93Ik8IA=="
	crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-router/4.0.12/vue-router.global.prod.min.js"
	integrity="sha512-V4wJo2tPFlrTfeFqrlPDJG/iKkcwX+RywJqiL/I5pfxucomceV5KC0Plqispv2Qav2KqZ8BfayOlsPTEhz5QAA=="
	crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<script src="https://cdn.jsdelivr.net/npm/chart.js@3.7.1/dist/chart.min.js"></script>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<!-- StyleSheets  -->
	<link rel="stylesheet" href="/./admin/assets/css/dashlite.css?ver=2.9.1">
	<link id="skin-default" rel="stylesheet" href="/./admin/assets/css/theme.css?ver=2.9.1">

</head>
