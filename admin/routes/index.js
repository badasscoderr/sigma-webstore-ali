/** Initilizing Vue Router */
const router = VueRouter.createRouter({
	history: VueRouter.createWebHistory("/"),
	routes: [
		{
			name: "Dashboard",
			path: "/admin/",
			component: Dashboard,
		},
		{
			name: "Products",
			path: "/admin/products",
			component: Products,
		},
		{
			name: "Orders",
			path: "/admin/orders",
			component: Orders,
		},
		{
			name: "Customers",
			path: "/admin/customers",
			component: Customers,
		},
		{
			name: "Customer",
			path: "/admin/customers/customer",
			component: Customer,
		},
		{
			name: "Order",
			path: "/admin/orders/order",
			component: Order,
		},
		{
			name: "Product",
			path: "/admin/products/product",
			component: Product,
		},
		{
			name: "Blank",
			path: "/admin/blank",
			component: Blank,
		}
	],
	scrollBehavior(to, from, savedPosition) {
		if (savedPosition) {
			return savedPosition;
		} else {
			return { top: 0 };
		}
	},
});
