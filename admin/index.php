<!DOCTYPE html>
<html lang="en">

<?php
	include('includes/head.php');
?>

<body>
	<div id="app"></div>

	<!-- Importing components -->
	<?php
		include('includes/components.php');
	?>

	<script src="/./admin/App.js"></script>
	<script src="/./admin/routes/index.js"></script>
	<script src="/./admin/main.js"></script>

	<?php
		include('includes/load-charts.php');
	?>
	<!-- <script type="module" src="/./admin/includes/components.js"></> -->

	<!-- JavaScript -->
</body>

</html>

