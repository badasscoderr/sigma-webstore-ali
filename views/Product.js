const Product = {
	template: /* html */ `
		<main>
			<section class="breadcrumb-area">
				<Breadcrumb :title="title" :itemActive="itemActive"/>
			</section>

			<section class="product-details-area pt-100px pb-100px">
				<ProductBody :products="products" />
			</section>

			<section class="description-review-area pb-100px">
				<ProductDesc :products="products" />
			</section>

			<section class="related-product-area">
				<RelatedProducts :products="products" />
			</section>
		</main>
	`,

	//props: ["id"],

	components: {
		ProductBody,
		ProductDesc,
		RelatedProducts,
	},

	computed: {
		...Vuex.mapGetters({
			products: "getProducts",
		}),
		product() {
			return this.products.find((x) => x.id === this.id);
		},
		title() {
			return this.product.title;
		}
	},

	data() {
		return {
			itemActive: "Product",

			id: Number(this.$route.params.id),
			//productId: null,
		};
	},

	beforeRouteEnter: (to, from) => {
		console.log("reeee");
		console.log(`${from.path} to ${to.path}?`);

		//const currentProduct = this.products.find((x) => x.id === to.params.id);
		//console.log(currentProduct);
	},

	mounted() {
		this.fetchProducts();
		//console.log(this.id);
		//console.log(this.product);
		//console.log("<-->", this.products);
		//console.log(this.$route.params.id);

	},

	methods: {
		...Vuex.mapActions(["fetchProducts"]),
	},
};
