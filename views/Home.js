const Home = {
	template: /* html */ `
		<main>
			<section class="section">
				<Slider />
			</section>
			<section class="banner-area pt-100px pb-100px">
				<Banner />
			</section>

			<section class="product-area">
				<ProductArea />
			</section>
			<section class="feature-area pt-100px">
				<FeatureArea />
			</section>
  	</main>

	`,

	/** Registering components */
	components: {
		Slider,
		Banner,
		ProductArea,
		FeatureArea,
		Footer,
	},
	data() {
		return {
			scTimer: 0,
			scY: 0,
		};
	},

	beforeRouteEnter: (to, from) => {
		/*
		 todo check if to === from
		 Warning!: location.reload()  completely destroy all vuejs stored states
		 */
		console.log("reeee");
		console.log(`${from.path} to ${to.path}?`);

		if (from.path !== to.path) {
			console.log("true they are not eq");
			setTimeout(() => {
				window.location.reload();
			}, 1000);
		} else {
			console.log("they are equal");
		}
	},

	/*beforeRouteLeave(to, from) {
		const answer = window.confirm(
			"Do you really want to leave? you have unsaved changes!"
		);
		this.reload = 0;
		console.log(this.reload);
		if (!answer) return false;
	},*/

	mounted() {},

	methods: {},
};
