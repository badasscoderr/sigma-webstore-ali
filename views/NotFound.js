const NotFound = {
	template: /* html */ `

		<!-- Blank area start -->
    <div class="blank-page-area pb-100px pt-100px" data-bg-image="../assets/images/404/404.jpg">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center">
						<div class="page-not-found text-center">
							<h2>Oops!</h2>
							<p>Sorry,Page Not Found.</p>
							<router-link to="/">
								Back To Home <i class="fa fa-home"></i>
							</router-link>
						</div>
					</div>
				</div>
			</div>
    </div>
    <!-- Blank area end -->

	`,
};
