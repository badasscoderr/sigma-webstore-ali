const Auth = {
	template: /* html */ `
		<div class="login-register-area pt-100px pb-100px">
			<div class="container">
				<div class="row">
					<div class="col-lg-7 col-md-12 ml-auto mr-auto">
						<div class="login-register-wrapper">
							<div class="login-register-tab-list nav">
								<router-link
									to=""
									@click="showLoginTab"
									:class="{'active': loginActive}"
								>
									<h4>Login</h4>
								</router-link>
								<router-link
									to=""
									@click="showRegisterTab"
									:class="{'active': registerActive}"
								>
										<h4>Register</h4>
								</router-link>
							</div>
							<div class="tab-content">
								<Login v-show="showLogin" />
								<Register v-show="showRegister" />
							</div>
						</div>
					</div>
				</div>
			</div>
    </div>
	`,

	components: { Register, Login },

	data() {
		return {
			loginActive: true,
			registerActive: false,
			showLogin: true,
			showRegister: false,
		};
	},

	computed: {},

	methods: {
		showLoginTab() {
			this.showLogin = true;
			this.showRegister = false;
			this.loginActive = true;
			this.registerActive = false;
		},
		showRegisterTab() {
			this.showLogin = false;
			this.showRegister = true;
			this.registerActive = true;
			this.loginActive = false;
		},
	},
};
