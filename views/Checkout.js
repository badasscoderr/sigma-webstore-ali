const Checkout = {
	template: /* html */ `
		<main>
			<section class="breadcrumb-area">
				<Breadcrumb :title="title" :itemActive="itemActive"/>
			</section>
			<section class="checkout-area pt-100px pb-100px">
    		<div class="container">
        	<div class="row">
						<BillingInfo />
						<OrderDetails :products="products" />
					</div>
				</div>
			</section>
		</main>
	`,

	components: { BillingInfo, OrderDetails },

	computed: {
		...Vuex.mapGetters({
			products: "getCart",
		}),
	},

	data() {
		return {
			title: "Checkout",
			itemActive: "Checkout",
			id: Number(this.$route.params.id),
		};
	},

	beforeRouteEnter: (to, from) => {
		/*
		 todo check if to === from
		 Warning!: location.reload()  completely destroy all vuejs stored states
		 */
		console.log("reeee");
		console.log(`${from.path} to ${to.path}?`);

		/*if (from.path !== to.path) {
			console.log("true they are not eq");

			return { name: "Login" };
		} else {
			console.log("they are equal");
		}*/
	},
};
