const Shop = {
	template: /* html */ `
		<main>
			<section class="breadcrumb-area">
				<Breadcrumb :title="title" :itemActive="itemActive"/>
			</section>

			<section class="shop-category-area pt-100px pb-100px">
				<div class="container">
					<div class="row">
						<div class="col-lg-9 order-lg-last col-md-12 order-md-first">
              <ShopBanner />

							<ShopTopArea
								:options="options"
								:selected="selected"
							 	@sending-limit="fetchProductsPerLimit"
							/>
							<MobileShopBar />

							<div class="shop-bottom-area">

								<div class="row">
									<div class="col">
										<div class="tab-content">
											<div class="tab-pane fade show active" id="shop-grid">
												<div class="row mb-n-30px">

													<div v-if="loading">Loading</div>
													<LargeProductCard
														v-for="product in products"
														:key="product"

														:product="product"
														:products="products"
														:title="product.title"
														:productid="product.id"
														:img="product.image"
														:price="product.price"
														:rating="product.rating.rate"
														v-else
													/>

												</div>
											</div>
										</div>
									</div>
								</div>

								<Pagination
									:products="products"
									:abc="abc"
									@sending-pagination="fetchProductsPerPage"
									@next-page="nextPage"
									@prev-page="prevPage"
								/>

								<!-- <LoadMore @loading-state="loadMore" /> --->

							</div>
						</div>

						<ShopSidebar
							@category-filter="categoryFilter"
							@clear-filters="clearFilters"
						/>

					</div>
				</div>
			</section>
		</main>
	`,

	components: {
		ShopTopArea,
		MobileShopBar,
		ShopSidebar,
		Pagination,
		LoadMore,
	},
	data() {
		return {
			title: "Shop",
			itemActive: "Shop",
			products: [],
			selectedVal: 10,
			loading: false,

			currentPage: 1,
			productsPerPage: 5,

			setProducts: [],
			currentProducts: [],
			indexOfLastProduct: 5,
			indexOfFirstProduct: 0,

			abc: "xyz",
		};
	},

	computed: {},

	mounted() {
		this.fetchProductsPerLimit();
	},
	methods: {
		fetchProductsPerLimit(selected) {
			this.loading = true;
			const url = `https://fakestoreapi.com/products?limit=${selected}`;
			if (
				selected != 15 &&
				selected != 20 &&
				selected != 25 &&
				selected != 50
			) {
				const url = `https://fakestoreapi.com/products?limit=10`;
				axios
					.get(url)
					.then((response) => {
						this.products = response.data;
						this.loading = false;
						console.log(selected);
						console.log(response.data);
					})
					.catch((error) => {
						console.log(error);
					});
			} else {
				axios
					.get(url)
					.then((response) => {
						this.products = response.data;
						this.loading = false;
						this.selectedVal = Number(selected);
						console.log(selected);
						console.log(response.data);
					})
					.catch((error) => {
						console.log(error);
					});
			}

			this.selected = selected;
		},

		fetchProductsPerPage({ currentPage, productsPerPage }) {
			this.loading = true;
			const url = `https://fakestoreapi.com/products`;
			axios
				.get(url)
				.then((response) => {
					this.setProducts = response.data;
					this.loading = false;
					setCurrentProducts = this.setProducts;
					this.currentProducts = setCurrentProducts.slice(
						this.indexOfFirstProduct,
						this.indexOfLastProduct
					);
					this.products = this.currentProducts;
					console.log(this.currentProducts);
					this.loading = false;
				})
				.catch((error) => {
					console.log(error);
				});
			this.productsPerPage = productsPerPage;
			this.currentPage = currentPage;
			this.indexOfLastProduct = this.currentPage * this.productsPerPage;
			this.indexOfFirstProduct = this.indexOfLastProduct - this.productsPerPage;

			console.log("clicked", currentPage);
			console.log("clicked", productsPerPage);

			console.log(this.indexOfLastProduct);
			console.log(this.indexOfFirstProduct);
		},

		nextPage({ currentPage, productsPerPage }) {
			this.loading = true;
			const url = `https://fakestoreapi.com/products`;
			axios
				.get(url)
				.then((response) => {
					this.setProducts = response.data;
					this.loading = false;
					setCurrentProducts = this.setProducts;
					this.currentProducts = setCurrentProducts.slice(
						this.indexOfFirstProduct,
						this.indexOfLastProduct
					);
					this.products = this.currentProducts;
					console.log(this.currentProducts);
					this.loading = false;
				})
				.catch((error) => {
					console.log(error);
				});

			this.productsPerPage = productsPerPage;
			this.currentPage = currentPage;
			this.indexOfLastProduct = this.currentPage * this.productsPerPage;
			this.indexOfFirstProduct = this.indexOfLastProduct - this.productsPerPage;

			console.log("clicked", currentPage);
			console.log("clicked", productsPerPage);

			console.log(this.indexOfLastProduct);
			console.log(this.indexOfFirstProduct);
		},
		prevPage({ currentPage, productsPerPage }) {
			this.loading = true;
			const url = `https://fakestoreapi.com/products`;
			axios
				.get(url)
				.then((response) => {
					this.setProducts = response.data;
					this.loading = false;
					setCurrentProducts = this.setProducts;
					this.currentProducts = setCurrentProducts.slice(
						this.indexOfFirstProduct,
						this.indexOfLastProduct
					);
					this.products = this.currentProducts;
					console.log(this.currentProducts);
					this.loading = false;
				})
				.catch((error) => {
					console.log(error);
				});

			this.productsPerPage = productsPerPage;
			this.currentPage = currentPage;
			this.indexOfLastProduct = this.currentPage * this.productsPerPage;
			this.indexOfFirstProduct = this.indexOfLastProduct - this.productsPerPage;

			console.log("clicked", currentPage);
			console.log("clicked", productsPerPage);

			console.log(this.indexOfLastProduct);
			console.log(this.indexOfFirstProduct);
		},

		categoryFilter(activeContent) {
			console.log("clicked--", activeContent);
			this.products = activeContent;
		},

		clearFilters(products) {
			this.products = products.slice(0, 10);
		},

		loadMore(loading) {
			perLoad = 10;
			loadProducts = this.selectedVal += perLoad;
			console.log(loadProducts);

			const url = `https://fakestoreapi.com/products?limit=${loadProducts}`;
			axios
				.get(url)
				.then((response) => {
					this.products = response.data;
					console.log(response.data);
					console.log(loading);
					loading = false;
				})
				.catch((error) => {
					console.log(error);
				});
		},
	},
};
