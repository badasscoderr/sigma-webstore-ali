const Cart = {
	template: /* html */ `
		<main v-show="showCart">
			<section class="breadcrumb-area">
				<Breadcrumb :title="title" :itemActive="itemActive"/>
			</section>

			<div class="cart-main-area
				pt-100px
				pb-100px"
			>
        <div class="container">
					<h3 class="cart-page-title">Your cart items</h3>
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-12">
							<form action="#">
								<div class="table-content table-responsive cart-table-content">
									<table>
										<TableHead />
										<TableBody
											v-for="product in products"
											:key="product.id"

											:showCartPane="showCartPane"
											:product="product"
											:productid="product.id"
											:title="product.title"
											:img="product.image"
											:price="product.price"
										/>
									</table>
								</div>

								<CartActions :showCartPane="showCartPane" />
							</form>
							<div class="row">
								<CartTotal />
							</div>
						</div>
					</div>
				</div>
			<div>
		</main>
		<EmptyCart v-if="emptyCart" />
	`,

	computed: {
		...Vuex.mapGetters({
			products: "getCart",
		}),
	},
	components: {
		TableHead,
		TableBody,
		CartActions,
		CartShipping,
		CartCoupon,
		CartTotal,
		EmptyCart,
	},

	data() {
		return {
			title: "Cart",
			itemActive: "Cart",
			showCart: true,
			emptyCart: false,
		};
	},

	mounted() {
		console.log(this.products);
		this.showCartPane();
	},

	methods: {
		showCartPane() {
			if (this.products.length > 0) {
				this.showCart = true;
				this.emptyCart = false;
			} else {
				this.showCart = false;
				this.emptyCart = true;
			}
			console.log("showCartPane called");
		},
	},
};
