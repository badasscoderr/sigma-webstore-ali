 <!-- Components Registration ----------------->
 <!-- Shared Components Registration --->
 <script src="../components/shared/navigation/account-action.js"></script>
 <script src="../components/shared/navigation/cart-sidebar.js"></script>
 <script src="../components/shared/navigation/mobile-menu.js"></script>
 <script src="../components/shared/navigation/search-modal.js"></script>
 <script src=".././components/shared/Navigation.js"></script>
 <script src=".././components/shared/Footer.js"></script>
 <script src="../components/shared/Breadcrumb.js"></script>
 <script src="../components/shared/ProductCard.js"></script>
 <script src="../components/shared/LargeProductCard.js"></script>

 <!-- Home components will be registered here ---->
 <script src=".././components/home/Slider.js"></script>
 <script src=".././components/home/Banner.js"></script>
 <script src=".././components/home/ProductArea.js"></script>
 <script src=".././components/home/FeatureArea.js"></script>

 <!--- Shop components will be registered here --->
 <script src="../components/shop/ShopTopArea.js"></script>
 <script src="../components/shop/MobileShopBar.js"></script>
 <script src="../components/shop/ShopSidebar.js"></script>
 <script src="../components/shop/Pagination.js"></script>
 <script src="../components/shop/LoadMore.js"></script>

 <!--- Product components will be registered here --->
 <script src="../components/product/ProductBody.js"></script>
 <script src="../components/product/ProductDesc.js"></script>
 <script src="../components/product/RelatedProducts.js"></script>

 <!--- Cart components will be registered here --->
 <script src="../components/cart/TableHead.js"></script>
 <script src="../components/cart/TableBody.js"></script>
 <script src="../components/cart/CartActions.js"></script>
 <script src="../components/cart/CartShipping.js"></script>
 <script src="../components/cart/CartCoupon.js"></script>
 <script src="../components/cart/CartTotal.js"></script>
 <script src="../components/cart/EmptyCart.js"></script>

 <!--- Checkout components will be registered here --->
 <script src="../components/checkout/BillingInfo.js"></script>
 <script src="../components/checkout/OrderDetails.js"></script>

 <!--- Auth components will be registered here --->
 <script src="../components/auth/Register.js"></script>
 <script src="../components/auth/Login.js"></script>


 <!--- All views will be rendered here --------->
 <script src="../views/Home.js"></script>
 <script src="../views/Shop.js"></script>
 <script src="../views/Product.js"></script>
 <script src="../views/Cart.js"></script>
 <script src="../views/Checkout.js"></script>
 <script src="../views/NotFound.js"></script>
 <script src="../views/Auth.js"></script>
 <script src="../views/ThankYou.js"></script>
 <script src="../views/Account.js"></script>
