<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $Title ?></title>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/3.2.26/vue.global.prod.min.js"
        integrity="sha512-yY2w8QVShzoLAachKPHtZRjXZeQOi9rQ2dYEYLf+lelt+TvZVOm/AlqVX6xFrjiy6wKDxgqvT1RL3BjxPdq/UA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vuex/4.0.2/vuex.global.prod.min.js"
        integrity="sha512-6qTR+Lfgwi+S5KFpSBVNXu/OXZszhGyc2Fw7a2yM07+k5EUkOQwcPdTr+FmyLb5A9MbQaQ5vhpoNIIj93Ik8IA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-router/4.0.12/vue-router.global.prod.min.js"
        integrity="sha512-V4wJo2tPFlrTfeFqrlPDJG/iKkcwX+RywJqiL/I5pfxucomceV5KC0Plqispv2Qav2KqZ8BfayOlsPTEhz5QAA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <!--Plugins JS-->
		<script src="/./assets/js/swiper-bundle.min.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://cdn.lordicon.com/lusqsztk.js"></script>
		<script src="https://unpkg.com/vue-dk-toast@3.0.0"></script>

    <!-- Add site Favicon -->
    <link rel="shortcut icon" href="assets/images/favicon/favicon.ico" type="image/png">

    <!-- vendor css (Icon Font) -->
    <link rel="stylesheet" href="/./assets/css/vendor/bootstrap.bundle.min.css" />
    <link rel="stylesheet" href="/./assets/css/vendor/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="/./assets/css/vendor/font.awesome.css" />

    <!-- plugins css (All Plugins Files) -->
    <link rel="stylesheet" href="/./assets/css/plugins/animate.css" />
    <link rel="stylesheet" href="/./assets/css/plugins/venobox.css" />
    <link rel="stylesheet" href="/./assets/css/plugins/swiper-bundle.min.css">
    <link rel="stylesheet" href="/./assets/css/plugins/price-filter.css">

    <!-- Main Style -->
    <link rel="stylesheet" href="/assets/css/style.css" />

</head>
