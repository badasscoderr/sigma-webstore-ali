const LargeProductCard = {
	template: /* html */ `
		<div
			class="col-lg-4
			col-md-6
			col-sm-6
			col-xs-6
			mb-30px"
			data-aos="fade-up"
			data-aos-delay="200"
		>
			<!-- Single Prodect -->
			<div class="product">
				<div class="thumb">
					<router-link
						:to="route"
						class="image"
					>
						<img
							:src="img"
							alt="Product"
						/>
					</router-link>
					<!-- <span class="badges">
						<span class="new">New</span>
					</span> -->
				</div>
				<div class="content">
					<span class="ratings">
						<span class="rating-wrap">
							<span class="star" style="width: 100%"></span>
						</span>
						<span class="rating-num d-none">( {{rating}} Review )</span>
					</span>
					<h5 class="title">
						<router-link :to="route">
							{{title}}
						</router-link>
					</h5>
					<span class="price">
						<span class="new">{{price}}</span>
					</span>
				</div>
				<button
					title="Add To Cart"
					class=" add-to-cart"
					@click="addToCart"
				>
					Add To Cart
				</button>
			</div>
		</div>
	`,

	props: [
		"id",
		"products",
		"product",
		"title",
		"productid",
		"img",
		"price",
		"rating",
	],

	computed: {
		route() {
			return `/products/${this.productid}`;
		},
	},

	data() {
		return {};
	},

	mounted() {
		//console.log(this.productid);
		//console.log(this.$route.params.id);
	},

	methods: {
		addToCart() {
			this.$store.commit("ADD_TO_CART", this.product);
			console.log("-> added to cart");
			this.$toast("Added to cart", {
				positionX: "right",
				positionY: "top",
				styles: {
					height: "30px",
					width: "250px",
				},
			});
		},
	},
};
