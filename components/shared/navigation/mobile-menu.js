const MobileMenu = {
	template: /* html */ `
		<div class="offcanvas offcanvas-mobile-menu offcanvas-open">
			<button class="offcanvas-close" @click="CloseMobileMenu"></button>

			<div class="inner customScroll">

				<div class="offcanvas-menu mb-4">
					<ul>
						<li @click="reload">
							<router-link to="/"><span class="menu-text">Home</span></router-link>
						</li>
						<li @click="reload">
							<router-link to="shop"><span class="menu-text">Shop</span></router-link>
						</li>
					</ul>
				</div>

			</div>
    </div>
	`,
	methods: {
		...Vuex.mapMutations(["TOGGLE_MOBILE_MENU"]),
		CloseMobileMenu() {
			this.TOGGLE_MOBILE_MENU();
		},
		reload() {
			console.log('mobile page is reloaded');
			window.location.reload();
		}
	}
};
