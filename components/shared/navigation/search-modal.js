const SearchModal = {
	template: /* html */ `
		<div class="modal popup-search-style show" style="display: block">
			<button type="button" @click="toggleCloseSearchModal" class="close-btn"><span>&times;</span></button>
			<div class="modal-overlay">
				<div class="modal-dialog p-0" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<h2>Search Your Product</h2>
							<form class="navbar-form position-relative" role="search">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Search here...">
								</div>
								<button type="submit" class="submit-btn"><i class="pe-7s-search"></i></button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	`,
	computed: {

	},
	methods: {
		...Vuex.mapMutations(["TOGGLE_SEARCH_MODAL"]),
		toggleCloseSearchModal() {
			this.TOGGLE_SEARCH_MODAL();
		},
	}
};
