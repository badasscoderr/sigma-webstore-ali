const AccountAction = {
	template: /* html */ `
		<ul class="dropdown-menu dropdown-menu-right show">
			<li>
				<router-link class="dropdown-item" to="">
					Login
				</router-link>
			</li>
			<li>
				<router-link class="dropdown-item" to="">
					My account
				</router-link>
			</li>
			<li>
				<router-link class="dropdown-item" to="">
					Checkout
				</router-link>
			</li>
		</ul>
	`,
};
