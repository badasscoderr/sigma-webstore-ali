const CartSidebar = {
	template: /* html */ `
		<div id="offcanvas-cart" class="offcanvas offcanvas-cart offcanvas-open">
			<div class="inner">
				<div class="head">
					<span class="title">Cart</span>
					<button class="offcanvas-close" @click="toggleShowCartSidebar">×</button>
				</div>

				<div class="body customScroll">
					<ul class="minicart-product-list">
						<div v-if="products.length > 0">
							<li v-for="product in products.slice(0, 4)" :key="product">
								<router-link to="" class="image">
									<img
										:src="product.image"
										alt="Cart product Image"
									>
								</router-link>
								<div class="content">
									<router-link to="" class="title">
										{{product.title}}
									</router-link>
									<span class="quantity-price">
										{{product.quantity}} x <span class="amount">$ {{product.price}}</span>
									</span>
									<router-link to="" class="remove" @click="removeFromCart(product)">×</router-link>
								</div>
							</li>
						</div>
						<div v-else>Nothing in cart</div>
					</ul>
				</div>
				<div class="foot">
					<div class="buttons mt-30px">
						<span @click="reload">
							<router-link to="/cart" class="btn btn-dark btn-hover-primary mb-30px">view cart</router-link>
						</span>
						<span @click="reload">
							<router-link to="/checkout" class="btn btn-outline-dark current-btn">checkout</router-link>
					  </span>
					</div>
				</div>
			</div>
		</div>

	`,

	computed: {
		...Vuex.mapGetters({ products: "getCart" }),
		route() {
			return `/products/${this.productid}`;
		},
	},

	data() {
		return {};
	},

	methods: {
		...Vuex.mapMutations(["TOGGLE_SEARCH_MODAL", "TOGGLE_CART_SIDEBAR"]),

		toggleShowCartSidebar() {
			this.TOGGLE_CART_SIDEBAR();
		},

		reload() {
			console.log("mobile page is reloaded");
			window.location.reload();
		},

		removeFromCart(product) {
			this.$store.commit("REMOVE_FROM_CART", product);
			this.$toast("Removed from cart", {
				positionX: "right",
				positionY: "top",
				styles: {
					height: "30px",
					width: "250px",
				},
			});
		},
	},

	mounted() {},
};
