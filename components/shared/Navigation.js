const Navigation = {
	template: /*html */ `
		<div class="header-main sticky-nav ">
			<div class="container position-relative">
				<div class="row">
					<div class="col-auto align-self-center">
						<div class="header-logo">
							<a href="/"><img src="https://raw.githubusercontent.com/jeanbaptis123/lib/main/sigma-logo-trans.png" alt="Site Logo" /></a>
						</div>
					</div>
					<div class="col align-self-center d-none d-lg-block">
						<div class="main-menu">
							<ul>
								<li><router-link to="/" >Home</router-link></li>
								<li><router-link to="/shop">Shop</router-link></li></li>
							</ul>
						</div>
					</div>
					<!-- Header Action Start -->
					<div class="col col-lg-auto align-self-center pl-0">
						<div class="header-actions">
							<!-- Search action -->
							<div class="header-action-btn" @click="ShowSearchModal">
									<i class="pe-7s-search"></i>
							</div>

							<!-- Account Action -->
							<div class="header-bottom-set dropdown">
								<button
									class="dropdown-toggle
									header-action-btn show"
									@click="showActionDropdown"
								><i class="pe-7s-users"></i>
								</button>
								<!-- Account Dropdown --->
								<AccountAction v-show="getActionDropdownState" />
							</div>

							<!-- Cart Action -->
							<div
								class="header-action-btn
								header-action-btn-cart
								offcanvas-toggle
								pr-0"
								@click="ShowCartSidebar"
								style="cursor: pointer"
							>
								<i class="pe-7s-shopbag"></i>
								<span class="header-action-num">01</span>
								<span class="cart-amount">€30.00</span>
							</div>

							<!--- Mobile Menu Toggle -->
							<div
								class="header-action-btn
								header-action-btn-menu
								offcanvas-toggle
								d-lg-none"
							  @click="ShowMobileMenu"
								style="cursor: pointer"
							>
								<i class="pe-7s-menu"></i>
							</div>
						</div>
						<!-- Header Action End -->
					</div>
				</div>
			</div>
		</div>

		<!-- Cart ModaL -->
		<transition name="slide-fade">
			<CartSidebar v-show="getCartSidebarState" />
		</transition>

		<!-- Mobile Menu -->
		<transition name="slide-fade">
			<MobileMenu v-show="getMobileMenuState" />
		</transition>

		<!-- Search Modal -->
    <SearchModal v-show="getModalState" />

	`,

	data() {
		return {};
	},

	computed: {
		...Vuex.mapGetters([
			"getModalState",
			"getCartSidebarState",
			"getMobileMenuState",
			"getActionDropdownState",
			"getUser",
		]),
	},

	mounted() {
		console.log(this.getUser);
	},

	methods: {
		...Vuex.mapMutations([
			"TOGGLE_SEARCH_MODAL",
			"TOGGLE_CART_SIDEBAR",
			"TOGGLE_MOBILE_MENU",
			"TOGGLE_ACTION_DROPDOWN",
		]),

		ShowSearchModal() {
			this.TOGGLE_SEARCH_MODAL();
		},
		ShowCartSidebar() {
			this.TOGGLE_CART_SIDEBAR();
			console.log("cart func is called");
		},
		ShowMobileMenu() {
			this.TOGGLE_MOBILE_MENU();
		},
		showActionDropdown() {
			this.TOGGLE_ACTION_DROPDOWN();
		},

		reload() {
			console.log("page is reloaded");
			window.location.reload();
		},
	},
};
