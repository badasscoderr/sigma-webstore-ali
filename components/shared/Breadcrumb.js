const Breadcrumb = {
	template: /* html */ `
		<main>
			<div class="container">
				<div class="row align-items-center justify-content-center">
					<div class="col-12 text-center">
						<h2 class="breadcrumb-title">{{title}}</h2>
						<!-- breadcrumb-list start -->
						<ul class="breadcrumb-list">
							<li class="breadcrumb-item">Home</li>
							<li class="breadcrumb-item active">{{itemActive}}</li>
						</ul>
						<!-- breadcrumb-list end -->
					</div>
				</div>
			</div>
		</main>


	`,
	props: {
		title: String,
		itemActive: String
	}
}
