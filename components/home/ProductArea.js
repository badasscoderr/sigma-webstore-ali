const ProductArea = {
	template: /* html */ `
		<!-- Product Area Start -->

		<div class="container">
			<!-- Section Title & Tab Start -->
			<div class="row">
				<!-- Section Title Start -->
				<div class="col-12">
					<div
						class="section-title
						text-center mb-60px"
					>
						<h2 class="title">Product Categories</h2>
					</div>

					<!-- Tab Start -->
					<div
						class="tab-slider
						swiper-container
						slider-nav-style-1
						small-nav category-carousel"
					>
						<div class="product-tab-nav nav swiper-wrapper">
							<div class="nav-item
								swiper-slide"
								v-for="(category, index) in categories"
								:key="index"
							>
								<div class="nav-link" @click="activeCategory = category">
									<img src="assets/images/icons/pottery-icon.png" alt="">
									<span>{{category}}</span>
								</div>
							</div>
							{{categories}}
						</div>
						<!-- Add Arrows -->
						<div class="swiper-buttons">
							<div class="swiper-button-next"></div>
							<div class="swiper-button-prev"></div>
						</div>
					</div>
					<!-- Tab End -->
				</div>
				<!-- Section Title End -->

			</div>
			<!-- Section Title & Tab End -->

			<div class="row">
				<div class="col">
					<div class="tab-content mt-60px">
						<!-- 1st tab start -->
						<div class="tab-pane fade show active">
							<div class="row">
								<ProductCard
									v-for="product in activeContent"
									:key="product"

									:products="products"
									:product="product"
									:title="product.title"
									:productid="product.id"
									:img="product.image"
									:price="product.price"
									:rating="product.rating.rate"
								/>
							</div>
						</div>
							<!-- 1st tab end -->
					</div>
				</div>
			</div>
		</div>

    <!-- Product Area End -->
	`,

	data() {
		return {
			activeCategory: "electronics",
		};
	},

	computed: {
		...Vuex.mapGetters({
			products: "getProducts",
			categories: "getCategories",
		}),
		activeContent() {
			return this.products.filter((x) => x.category === this.activeCategory);
		},
	},
	mounted() {
		this.fetchProducts();
		this.fetchCategories();
		console.log("categories", this.categories);
		console.log("products", this.products);
		console.log("activeContent", this.activeContent);
	},
	methods: {
		...Vuex.mapActions(["fetchProducts", "fetchCategories"]),
		reload() {
			console.log("page is reloaded");
			window.location.reload();
		},
	},
};
