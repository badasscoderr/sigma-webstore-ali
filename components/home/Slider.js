const Slider = {
	template: /* html */ `
		<div class="main-slider swiper main-slider">
			<div class="swiper-wrapper">
				<div class="swiper-slide" v-for="(slide, index) in slidess"
					:key="index">

						<div class="hero-slide-item
							slider-height
							swiper-slide
							d-flex bg-color1"
						>
							<div class="container align-self-center">
								<div class="row">
									<div class="col-xl-6
										col-lg-6
										col-md-6
										col-sm-6
										align-self-center
										sm-center-view"
									>
										<div class="hero-slide-content">
											<h2 class="title-1
												helperbird-editor
												medium-editor-element"
											>
												Best Handmade Goods
											</h2>
											<span class="price
												helperbird-editor
												medium-editor-element"
											>
												<span class="old
													helperbird-editor
													medium-editor-element"
												> <del>$25.00</del>
												</span>
												<span class="new
													helperbird-editor
													medium-editor-element"
												> - {{slide.price}}
												</span>
											</span>
											<a class="btn btn-primary m-auto text-uppercase">
												View Collection
											</a>
										</div>
									</div>
									<div class="col-xl-6
										col-lg-6
										col-md-6
										col-sm-6
										d-flex
										justify-content-center
										position-relative"
									>
										<div class="show-case">
											<div class="">
												<img src="assets/images/slider-image/slider-1.png" alt="" />
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
				</div>
			</div>
		</div>
	`,

	data() {
		return {
			slidess: [],
			slides: [
				{
					id: 1,
					title: "Best Handmade Goods",
					imageUrl: "../../assets/images/slider-image/slider-1.png",
					newPrice: "$25.00",
					oldPrice: "$18.00",
				},
				{
					id: 2,
					title: "Best Cotton",
					imageUrl: "../../assets/images/slider-image/slider-2.png",
					newPrice: "$25.00",
					oldPrice: "$18.00",
				},
			],
		};
	},
	computed: {},
	mounted() {
		this.fetchSlides();
	},
	methods: {
		async fetchSlides() {
			try {
				const url = "https://fakestoreapi.com/products";
				const response = await axios.get(url);

				this.slidess = response.data;
			} catch (err) {
				if (err.response) {
					// client received an error response (5xx, 4xx)
					console.log("Server Error:", err);
				} else if (err.request) {
					// client never received a response, or request never left
					console.log("Network Error:", err);
				} else {
					console.log("Client Error:", err);
				}
			}
		},
	},
};
