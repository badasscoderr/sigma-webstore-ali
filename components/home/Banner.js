const Banner = {
	template: /* html */ `

		<div class="container">
			<div class="row">
				<div class="single-col">
					<div class="single-banner">
						<img src="assets/images/banner/1.jpg" alt="">
						<div class="banner-content">
							<span class="category">Best Seller</span>
							<span class="title">Flower Vase <br>& Poot</span>
							<a href="shop-left-sidebar.html" class="shop-link btn btn-primary text-uppercase">Shop
								Now</a>
						</div>
					</div>
				</div>
				<div class="single-col center-col">
						<div class="single-banner">
								<img src="assets/images/banner/2.jpg" alt="">
								<div class="banner-content">
										<span class="category">Best Seller</span>
										<span class="title">Wool Silk Dress <br>
												& Offer 2021</span>
										<a href="shop-left-sidebar.html" class="shop-link btn btn-primary text-uppercase">Shop
												Now</a>
								</div>
						</div>
				</div>
				<div class="single-col">
						<div class="single-banner">
								<img src="assets/images/banner/3.jpg" alt="">
								<div class="banner-content">
										<span class="category">Best Seller</span>
										<span class="title">Pen Holder<br>
												& Poot</span>
										<a href="shop-left-sidebar.html" class="shop-link btn btn-primary text-uppercase">Shop
												Now</a>
								</div>
						</div>
				</div>
			</div>
		</div>


	`
};
