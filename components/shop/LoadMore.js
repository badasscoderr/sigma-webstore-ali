const LoadMore = {
	template: /* html */ `
		<div class="pro-pagination-style text-center text-lg-end" data-aos="fade-up" data-aos-delay="200">
			<div class="pages">
				<button type="button" @click="getLoadingState" class="btn btn-dark p-3 load-more-btn">
					<lord-icon
						src="https://cdn.lordicon.com/ymrqtsej.json"
						trigger="loop"
						colors="primary:#ffffff,secondary:#08a88a"
						style="width:50px;height:50px"
						v-if="loading"
					>
					</lord-icon>
					<span v-else class="load-more-text">Load more</span>
				</button>
			</div>
		</div>
	`,

	data() {
		return {
			loading: false,
		};
	},

	methods: {
		getLoadingState() {
			this.$emit("loading-state", this.loading);
			this.loading = true;

			setTimeout(() => {
				this.loading = false;
			}, 3000);
		},
	},
};
