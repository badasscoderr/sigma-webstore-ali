const Pagination = {
	template: /* html */ `
		<div class="pro-pagination-style text-center text-lg-end" data-aos="fade-up" data-aos-delay="200">
			<div class="pages">
				<ul>
					<li class="li" @click="prev">
						<router-link class="page-link" to="">
							<i class="fa fa-angle-left"></i>
						</router-link>
					</li>
					<li class="li pagesEl" v-for="(page, index) in pages" :key="index">
						<router-link
							class="page-link"
							to=""
							@click="perPage(page)"
							:class="{'active': currentPage === page}"
						>
							{{page}}
						</router-link>
					</li>
					<li class="li" @click="next">
						<router-link class="page-link" to="">
							<i class="fa fa-angle-right"></i>
						</router-link>
					</li>
				</ul>
			</div>
		</div>
	`,

	computed: {
		abc() {
			return "xyz";
		},
	},

	data() {
		return {
			isActive: false,
			currentPage: 1,
			currentPosition: 1,
			innerPosition: 1,
			pages: [],
			productsPerPage: 5,
			products: 5,
		};
	},

	mounted() {
		this.fetchProducts();

		setTimeout(() => {
			this.getPagination();
		}, 5000);
	},

	methods: {
		...Vuex.mapActions(["fetchProducts"]),

		getPagination() {
			totalProducts = this.$store.state.products.length;
			totalPages = totalProducts / this.productsPerPage;

			console.log(totalProducts);
			console.log("log");

			const start = 1;
			const end = totalPages;
			const totalPagesArr = [...Array(end - start + 1).keys()].map(
				(x) => x + start
			);

			this.pages = totalPagesArr;
		},

		perPage(pageNumber) {
			this.currentPage = pageNumber;

			// emitting currentPage number
			this.$emit("sending-pagination", {
				currentPage: this.currentPage,
				productsPerPage: this.productsPerPage,
			});
		},

		next() {
			if (this.currentPage < this.pages.length) {
				this.currentPage += 1;
				console.log("->--", this.currentPage);
			} else {
				console.log("default", this.currentPage);
			}

			this.$emit("next-page", {
				currentPage: this.currentPage,
				productsPerPage: this.productsPerPage,
			});
		},

		prev() {
			if (this.currentPage !== 1) {
				this.currentPage -= 1;
				console.log("->--", this.currentPage);
			} else {
				console.log("default", this.currentPage);
			}

			this.$emit("prev-page", {
				currentPage: this.currentPage,
				productsPerPage: this.productsPerPage,
			});
		},
	},
};
