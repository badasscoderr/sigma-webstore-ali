const ShopTopArea = {
	template: /* html */ `
		<div class="desktop-tab">
			<div class="shop-top-bar d-flex">
				<!-- Right Side Start -->
				<div class="select-shoing-wrap d-flex align-items-center">
					<div class="shot-product">
						<p>Show:</p>
					</div>
					<div class="shop-select show">
						<select
							class="form-control
							form-control-lg"
							v-model="selected"
							@change="getLimit"
						>
							<option
								v-for="option in options"
								:key="option"
							>
								{{option}}
							</option>
						</select>
					</div>
				</div>
			</div>
		</div>
	`,

	props: [],
	data() {
		return {
			selected: 10,
			options: [10, 15, 20, 25, 50],
		};
	},

	methods: {
		getLimit() {
			this.$emit("sending-limit", this.selected);
		},
	},
};
