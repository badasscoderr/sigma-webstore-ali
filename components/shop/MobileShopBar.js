const MobileShopBar = {
	template: /* html */ `
		<!-- Mobile shop bar -->
		<div class="shop-top-bar mobile-tab">
			<!-- Left Side End -->
			<div class="shop-tab nav d-flex justify-content-between">
				<!-- Right Side Start -->
				<div class="select-shoing-wrap d-flex align-items-center">
					<div class="shot-product">
							<p>Sort By:</p>
					</div>
					<div class="shop-select">
						<select class="shop-sort">
							<option data-display="Default">Default</option>
							<option value="3"> Price, low to high</option>
							<option value="4"> Price, high to low</option>
						</select>
					</div>
				</div>
			</div>
			<!-- Right Side End -->
			<!-- Right Side Start -->
			<div class="select-shoing-wrap d-flex align-items-center justify-content-between">
				<div class="select-shoing-wrap d-flex align-items-center">
					<div class="shot-product">
							<p>Show:</p>
					</div>
					<div class="shop-select show">
						<select class="shop-sort">
							<option data-display="12">12</option>
							<option value="1"> 12</option>
							<option value="2"> 10</option>
							<option value="3"> 25</option>
							<option value="4"> 20</option>
						</select>
					</div>
				</div>
				<!-- Right Side End -->
				<!-- Left Side start -->
				<p class="compare-product">Product Compare <span>(0) </span></p>
			</div>
		</div>
		<!-- Mobile shop bar -->
	`,
};
