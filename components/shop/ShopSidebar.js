const ShopSidebar = {
	template: /* html */ `
		<div class="col-lg-3 order-lg-first col-md-12 order-md-last">
			<div class="shop-sidebar-wrap">
				<!-- Sidebar single item -->
				<div class="sidebar-widget">
					<h4 class="sidebar-title">Categories</h4>
					<div class="sidebar-widget-category">
						<ul>
							<li v-for="category in categories" :key="category">
								<router-link
									to=""
									class="selected m-0"
									@click="setCategory(category)"
									:class="{'active-filter': activeCategory === category}"
								>
									<i class="fa fa-angle-right"></i>
									{{category}}
								</router-link>
							</li>
						</ul>
					</div>
				</div>

				<!-- Sidebar single item -->
				<div class="sidebar-widget mt-8">
					<h4 class="sidebar-title">Price Filter</h4>
					<div class="price-filter">
						<div class="price-slider-amount price-range">
						<span>Set price range from 30 USD to 500 USD</span>
						<br><br>
							<input
								type="range"
								id="amount"
								class="p-0 h-auto lh-1"
								name="price"
								placeholder="Add Your Price"
								v-model.number="setPrice"
								min="30" max="500"
								@change="setPriceFilter"
							/>
						</div>
						<br>
						{{setPrice}}
						<div id="slider-range"></div>
					</div>
				</div>

				<!-- Sidebar single item -->
				<div class="sidebar-widget mt-8">
					<h4 class="sidebar-title">Price Filter</h4>

				</div>

				<!-- Reset filters -->
				<div class="sidebar-widget">
					<h4 class="sidebar-title">Reset</h4>
					<div class="sidebar-widget-category">
						<ul>
							<li>
								<router-link to="" class="selected m-0" @click="clearFilters">
									Clear filters
								</router-link>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

	`,
	components: {},

	data() {
		return {
			activeCategory: null,
			setPrice: 100,
		};
	},

	computed: {
		...Vuex.mapGetters({
			products: "getProducts",
			categories: "getCategories",
		}),

		activeContent() {
			return this.products.filter((x) => x.category === this.activeCategory);
		},
		productsByPrice() {
			return this.products.filter((x) => x.price === this.setPrice);
		},
	},

	mounted() {
		this.fetchProducts();
		this.fetchCategories();
		/* setTimeout(() => {
			console.log(this.activeContent);
			console.log(this.pro);
		}, 7000); */
	},

	methods: {
		...Vuex.mapActions(["fetchProducts", "fetchCategories"]),

		setCategory(category) {
			this.activeCategory = category;

			if (this.activeCategory === category) {
				console.log("this is true");
			} else {
				console.log("this is false");
			}
			this.$emit("category-filter", this.activeContent);
		},

		setPriceFilter() {
			console.log(this.setPrice);
			console.log(this.products);
			console.log(this.productsByPrice);
		},

		clearFilters() {
			this.$emit("clear-filters", this.products);
		},
	},
};
