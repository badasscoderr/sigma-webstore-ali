const ProductBody = {
	template: /* html */ `

		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-sm-12 col-xs-12 mb-lm-30px mb-md-30px mb-sm-30px">
					<!-- Swiper -->
					<div class="swiper-wrapper">
						<div class="zoom-image-hover">
							<img class="img-responsive m-auto"
								:src="product.image"
								alt=""
							>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-sm-12 col-xs-12" data-aos="fade-up" data-aos-delay="200">
					<div class="product-details-content quickview-content ml-25px">
						<h2>{{product.title}}</h2>
						<div class="pricing-meta">
							<ul class="d-flex">
								<li class="new-price">$ {{product.price}}</li>
							</ul>
							{{product_total}}
						</div>
						<div class="pro-details-rating-wrap">
							<div class="rating-product">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
							</div>
							<span class="read-review"><a class="reviews" href="#">( 2 Review )</a></span>
						</div>
						<div class="stock mt-30px">
							<span class="avallabillty">
								Availability:
								<span class="in-stock">
									<i class="fa fa-check"></i>
									In Stock
								</span>
							</span>
						</div>

						<div class="pro-details-quality">
							<div class="cart-plus-minus" v-if="product_total > 0">
								<div class="dec qtybutton" @click="cartDecrement"> - </div>
								<input
									class="cart-plus-minus-box"
									type="text" name="qtybutton"
									v-model="product_total"
								/>
								<div class="inc qtybutton" @click="cartIncrement"> + </div>
							</div>
							<div class="pro-details-cart">
								<button class="add-cart" @click="addToCart">
									Add To Cart
								</button>
							</div>
						</div>
						<div class="pro-details-categories-info pro-details-same-style d-flex">
							<span>Categories: </span>
							<ul class="d-flex">
								<li>
									<router-link to=""> {{product.category}}</router-link>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	`,

	props: ["products"],

	computed: {
		product() {
			return this.products.find((x) => x.id === this.id);
		},
		product_total() {
			return this.$store.getters.getProductQty(this.product);
		},
	},

	data() {
		return {
			id: Number(this.$route.params.id),
		};
	},

	mounted() {
		//this.fetchProducts();
		//console.log(this.product);
		//console.log("<-->", this.products);
		//console.log(this.$route.params.id);
	},

	methods: {
		//...Vuex.mapActions(["fetchProducts"]),

		addToCart() {
			this.$store.commit("ADD_TO_CART", this.product);
			this.$toast("Added to cart", {
				positionX: "right",
				positionY: "top",
				styles: {
					height: "30px",
					width: "250px",
				},
			});
		},
		removeFromCart() {
			this.$store.commit("REMOVE_FROM_CART", this.product);
			this.$toast("Removed from cart", {
				positionX: "right",
				positionY: "top",
				styles: {
					height: "30px",
					width: "250px",
				},
			});
		},

		cartIncrement() {
			this.addToCart();
		},
		cartDecrement() {
			this.removeFromCart();
		},
	},
};
