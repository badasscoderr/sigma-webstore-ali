const RelatedProducts = {
	template: /* html */ `
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="section-title text-center line-height-1">
						<h2 class="title">Related Products</h2>

					</div>
				</div>
			</div>

			<div class="related-product-carousel
				new-product-slider
				swiper-container
				slider-nav-style-1
				pb-100px"
			>
				<div class="swiper-wrapper">
					<!-- Slides -->
					<div class="swiper-slide">Slide 1</div>
					<div class="swiper-slide">Slide 2</div>
					<div class="swiper-slide">Slide 3</div>

				</div>
				<!-- Add Arrows
				<div class="swiper-buttons">
						<div class="swiper-button-next"></div>
						<div class="swiper-button-prev"></div>
				</div>-->
			</div>
		</div>
	`,
	props: ["products"],

	data() {
		return {
			id: Number(this.$route.params.id),
			productId: null,
		};
	},

	computed: {
		product() {
			return this.products.find((x) => x.id === this.id);
		},
		RelatedProducts() {
			return this.products.filter((x) => x.category === this.product.category);
		},
	},

	mounted() {
		//console.log(this.product);
		//console.log("<-->", this.products);
		//console.log(this.$route.params.id);
		console.log("-<><>", this.RelatedProducts);
	},
};
