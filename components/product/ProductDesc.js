const ProductDesc = {
	template: /* html */ `
		<div class="container">
			<div class="description-review-wrapper">
				<div class="description-review-topbar nav">
					<router-link to="" class="active">Information</router-link>
					<router-link to="" >Description</router-link>
					<router-link to="" >Reviews</router-link>
				</div>
				<div class="tab-content description-review-bottom">
					<div class="tab-pane active">
						<div class="product-anotherinfo-wrapper text-start">
							<ul>
								<li><span>Weight</span> 400 g</li>
								<li><span>Dimensions</span>10 x 10 x 15 cm</li>
								<li><span>Materials</span> 60% cotton, 40% polyester</li>
								<li><span>Other Info</span> American heirloom jean shorts pug seitan letterpress</li>
							</ul>
						</div>
					</div>
					<div class="tab-pane">
						<div class="product-description-wrapper">
							<p>{{product.description}}</p>
						</div>
					</div>
					<div class="tab-pane ">
						<div>In progress</div>
						<!--
						<div class="row">
							<div class="col-lg-7">
								<div class="review-wrapper">
									<div class="single-review">
										<div class="review-img">
											<img src="assets/images/review-image/1.png" alt="" />
										</div>
										<div class="review-content">
											<div class="review-top-wrap">
												<div class="review-left">
													<div class="review-name">
														<h4>White Lewis</h4>
													</div>
													<div class="rating-product">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
													</div>
												</div>
												<div class="review-left">
													<a href="#">Reply</a>
												</div>
											</div>
											<div class="review-bottom">
												<p>
													Vestibulum ante ipsum primis aucibus orci luctustrices posuere
													cubilia Curae Suspendisse viverra ed viverra. Mauris ullarper
													euismod vehicula. Phasellus quam nisi, congue id nulla.
												</p>
											</div>
										</div>
									</div>
									<div class="single-review child-review">
										<div class="review-img">
											<img src="assets/images/review-image/2.png" alt="" />
										</div>
										<div class="review-content">
											<div class="review-top-wrap">
												<div class="review-left">
													<div class="review-name">
														<h4>White Lewis</h4>
													</div>
													<div class="rating-product">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
													</div>
												</div>
												<div class="review-left">
													<a href="#">Reply</a>
												</div>
											</div>
											<div class="review-bottom">
													<p>Vestibulum ante ipsum primis aucibus orci luctustrices posuere
															cubilia Curae Sus pen disse viverra ed viverra. Mauris ullarper
															euismod vehicula.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-5">
								<div class="ratting-form-wrapper pl-50">
									<h3>Add a Review</h3>
									<div class="ratting-form">
										<form action="#">
											<div class="star-box">
												<span>Your rating:</span>
												<div class="rating-product">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<div class="rating-form-style">
														<input placeholder="Name" type="text" />
													</div>
												</div>
												<div class="col-md-6">
													<div class="rating-form-style">
														<input placeholder="Email" type="email" />
													</div>
												</div>
												<div class="col-md-12">
													<div class="rating-form-style form-submit">
														<textarea name="Your Review" placeholder="Message"></textarea>
														<button class="btn btn-primary btn-hover-color-primary "
															type="submit" value="Submit">Submit</button>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
						-->
					</div>
				</div>
			</div>
		</div>
	`,

	props: ["products"],

	data() {
		return {
			id: Number(this.$route.params.id),
			productId: null,
		};
	},

	computed: {
		product() {
			return this.products.find((x) => x.id === this.id);
		},
	},

	mounted() {
		//console.log(this.product);
		//console.log("<-->", this.products);
		//console.log(this.$route.params.id);
	},

	methods: {},
};
