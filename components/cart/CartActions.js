const CartActions = {
	template: /* html */ `
		<div class="row">
			<div class="col-lg-12">
					<div class="cart-shiping-update-wrapper">
							<div class="cart-shiping-update">
								<router-link to="/shop">Continue Shopping</router-link>
							</div>
							<div class="cart-clear">
								<button type="button" @click="clearCart">Clear Shopping Cart</button>
							</div>
					</div>
			</div>
		</div>
	`,

	props: ["showCartPane"],

	methods: {
		clearCart() {
			this.$store.commit("CLEAR_CART");
			this.$toast("Clearing Cart", {
				positionX: "right",
				positionY: "top",
				styles: {
					height: "30px",
					width: "250px",
				},
			});
			//console.log(this.showCartPane);
			this.showCartPane();
		},
	},
};
