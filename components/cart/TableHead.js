const TableHead = {
	template: /* html */ `
		<thead>
			<tr>
				<th>Image</th>
				<th>Product Name</th>
				<th>Until Price</th>
				<th>Qty</th>
				<th>Subtotal</th>
				<th>Action</th>
			</tr>
		</thead>
	`,
};
