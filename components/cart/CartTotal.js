const CartTotal = {
	template: /* html */ `
		<div class="col-lg-4 col-md-12 mt-md-30px">
			<div class="grand-totall">
				<div class="title-wrap">
						<h4 class="cart-bottom-title section-bg-gary-cart">Cart Total</h4>
				</div>
				<h5>Total products <span>$ {{ cart_total.toFixed(2) }}</span></h5>

				<h4 class="grand-totall-title">Grand Total <span>$260.00</span></h4>
				<router-link to="/checkout">Proceed to Checkout</router-link>
			</div>
		</div>
	`,

	computed: {
		cart_total() {
			return this.$store.getters.getCart.reduce(
				(a, b) => a + b.price * b.quantity,
				0
			);
		},
	},
};
