const EmptyCart = {
	template: /* html */ `
		<!-- Cart area start -->
    <div class="empty-cart-area pb-100px pt-100px">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="empty-text-contant text-center">
							<i class="pe-7s-shopbag"></i>
							<h3>There are no more items in your cart</h3>
							<router-link :to="route" class="empty-cart-btn" href="shop-4-column.html">
								<i class="fa fa-arrow-left"> </i>
								Continue shopping
							</router-link>
						</div>
					</div>
				</div>
			</div>
    </div>
    <!-- Cart area end -->
	`,


	computed: {
		route() {
			return `/shop`;
		},
	},
};
