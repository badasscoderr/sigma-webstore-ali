const TableBody = {
	template: /* html */ `
		<tbody>
			<tr>
				<td class="product-thumbnail">
					<router-link :to="route" >
						<img
							class="img-responsive
							ml-15px"
							:src="img"
							alt=""
						/>
					</router-link>
				</td>
				<td class="product-name">
					<router-link :to="route">
						{{title}}
					</router-link>
				</td>
				<td class="product-price-cart">
					<span class="amount">$ {{price}}</span>
				</td>
				<td class="product-quantity">
					<div class="pro-details-quality">
						<div class="cart-plus-minus" v-if="product_total > 0">
							<div class="dec qtybutton" @click="cartDecrement"> - </div>
							<input
								class="cart-plus-minus-box"
								type="text" name="qtybutton"
								v-model="product_total"
							/>
							<div class="inc qtybutton" @click="cartIncrement"> + </div>
						</div>
					</div>
				</td>
				<td class="product-subtotal">$ {{subTotal}}</td>
				<td class="product-remove">
					<button type="button" @click="removeFromCart">
						<i class="fa fa-times"></i>
					</button>
				</td>
			</tr>

		</tbody>

	`,
	props: [
		"products",
		"product",
		"productid",
		"title",
		"img",
		"price",
		"showCartPane",
	],

	computed: {
		route() {
			return `/products/${this.productid}`;
		},
		product_total() {
			return this.$store.getters.getProductQty(this.product);
		},
		subTotal() {
			return this.product_total * this.price;
		},
	},

	methods: {
		addToCart() {
			this.$store.commit("ADD_TO_CART", this.product);
			this.$toast("Added to cart", {
				positionX: "right",
				positionY: "top",
				styles: {
					height: "30px",
					width: "250px",
				},
			});
		},
		removeFromCart() {
			this.$store.commit("REMOVE_FROM_CART", this.product);
			this.$toast("Removed from cart", {
				positionX: "right",
				positionY: "top",
				styles: {
					height: "30px",
					width: "250px",
				},
			});
			this.showCartPane();
		},

		cartIncrement() {
			this.addToCart();
		},
		cartDecrement() {
			this.removeFromCart();
		},
	},

	mounted() {
		console.log("--.", this.product_total);
	},
};


