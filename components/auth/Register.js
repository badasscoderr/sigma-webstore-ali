const Register = {
	template: /* html */ `

		<div class="login-form-container">
			<div class="login-register-form">
			{{credentials}}
				<form method="post" class="row" @submit.prevent="onRegister">
					<div class="col-lg-6 col-md-6">
						<input type="text" name="first-name" placeholder="Firstname" v-model="user_firstname" />
					</div>
					<div class="col-lg-6 col-md-6">
						<input type="text" name="last-name" placeholder="Lastname" v-model="user_lastname" />
					</div>
					<div class="col-lg-12">
						<input type="text" name="user-name" placeholder="Username" v-model="user_name" />
					</div>
					<div class="col-lg-12">
						<input name="user-email" placeholder="Email" type="email" v-model="user_email" />
					<div>
					<div class="col-lg-12">
						<input type="password" name="user-password" placeholder="Password" v-model="user_password" />
					</div>
					<br><hr><br>
					<label><small>These fields are optionals</small></label>
					<div class="col-lg-12">
						<input type="text" name="user-city" placeholder="City" v-model="user_city" />
					<div>
					<div class="col-lg-12">
						<input type="text" name="user-zipcode" placeholder="Zipcode" v-model="user_zipcode" />
					<div>
					<div class="col-lg-12">
						<input type="text" name="user-street" placeholder="Street" v-model="user_street" />
					<div>
					<div class="col-lg-12">
						<input type="text" name="user-house" placeholder="House #" v-model="user_house" />
					<div>
					<div class="col-lg-12">
						<input type="text" name="user-lat" placeholder="Latitude" v-model="user_lat" />
					<div>
					<div class="col-lg-12">
						<input type="text" name="user-long" placeholder="Longitude" v-model="user_long" />
					<div>
					<div class="col-lg-12">
						<input type="text" name="user-long" placeholder="Phone Number" v-model="user_phone" />
					<div>

					<div class="button-box">
						<button><span>Register</span></button>
					</div>
				</form>
			</div>
		</div>

	`,

	data() {
		return {
			user_firstname: "",
			user_lastname: "",
			user_name: "",
			user_email: "",
			user_password: "",

			user_city: "",
			user_zipcode: "",
			user_street: "",
			user_house: "",
			user_lat: "",
			user_long: "",
			user_phone: "",
		};
	},

	mounted() {},

	methods: {
		...Vuex.mapActions(["registerUser"]),
		onRegister() {
			const credentials = {
				fistname: this.user_firstname,
				lastname: this.user_lastname,
				username: this.user_name,
				email: this.user_email,
				password: this.user_password,

				city: this.user_city,
				zipcode: this.user_zipcode,
				street: this.user_street,
				house: this.user_house,
				lat: this.user_lat,
				long: this.user_long,
				phone: this.user_phone,
			};
			/* this.registerUser({
				fistname: this.user_firstname,
				lastname: this.user_lastname,
				username: this.user_name,
				email: this.user_email,
				password: this.user_password,

				city: this.user_city,
				zipcode: this.user_zipcode,
				street: this.user_street,
				house: this.user_house,
				lat: this.user_lat,
				long: this.user_long,
				phone: this.user_phone,
			}).then(() => {
				//this.$router.push({ name: "dashboard" });
			}); */
			//console.log(credentials);

			console.log("sign up pressed");
			//axios.post("https://fakestoreapi.com/users", credentials);
			fetch("https://fakestoreapi.com/users", {
				method: "POST",
				body: JSON.stringify({
					email: "John@gmail.com",
					username: "johnd",
					password: "m38rmF$",
					name: {
						firstname: "John",
						lastname: "Doe",
					},
					address: {
						city: "kilcoole",
						street: "7835 new road",
						number: 3,
						zipcode: "12926-3874",
						geolocation: {
							lat: "-37.3159",
							long: "81.1496",
						},
					},
					phone: "1-570-236-7033",
				}),
			})
				.then((res) => res.json())
				.then((json) => console.log(json));
		},
	},
};
