const Login = {
	template: /* html */ `
		<div class="login-form-container">
			<div class="login-register-form">
				<form method="post" @submit.prevent="onLogin" >
					<input type="text" name="user-name" placeholder="Username" v-model="user_name" />
					<input type="password" name="user-password" placeholder="Password" v-model="user_password" />
					<div class="button-box">
						<div class="login-toggle-btn">
							<router-link to="">Forgot Password?</router-link>
						</div>
						<button><span>Login</span></button>
					</div>
				</form>
			</div>
		</div>
	`,

	data() {
		return {
			user_name: "johnd",
			user_password: "m38rmF$",
		};
	},

	methods: {
		...Vuex.mapActions(["loginUser"]),
		onLogin() {
			const credentials = {
				username: this.user_name,
				password: this.user_password,
			};
			this.loginUser(credentials)
				.then(() => {
					this.$router.push({ name: "Account" });
				})
				.catch((err) => {
					this.status = err.response.status;
				});

			console.log("onLogin");
		},
	},
};

