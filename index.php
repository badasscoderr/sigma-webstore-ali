<!DOCTYPE html>
<html lang="en">

<?php
	include('includes/head.php');
?>

<body>
    <div id="app"></div>

    <!-- Importing components -->
    <?php
			include('includes/components.php');
		?>

		<script src="/./routes/index.js"></script>
		<script src="/./store/index.js"></script>
    <script src="/App.js"></script>

    <script>
    // Using Vuex Store
    app.use(store);

    // Using Vue Router
    app.use(router);

		//
		app.use(DKToast)

    // Mounting vue app
    app.mount('#app');
    </script>

    <?php
			include('includes/load-swiper.php');
		?>
</body>

</html>
